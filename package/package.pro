DISTFILES = config.xml \
    focus.png normal.png name.png \
    kids_focus.png \
    kids_normal.png \
    focus_dis.png


copy_icon.target = $$OUT_PWD/root/normal.png
copy_icon.depends = $$_PRO_FILE_PWD_/normal.png
copy_icon.commands = $(COPY_FILE) \"$$replace(copy_icon.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_icon.target, /, $$QMAKE_DIR_SEP)\"
QMAKE_EXTRA_TARGETS += copy_icon
PRE_TARGETDEPS += $$copy_icon.target

copy_icon1.target = $$OUT_PWD/root/focus.png
copy_icon1.depends = $$_PRO_FILE_PWD_/focus.png
copy_icon1.commands = $(COPY_FILE) \"$$replace(copy_icon1.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_icon1.target, /, $$QMAKE_DIR_SEP)\"
QMAKE_EXTRA_TARGETS += copy_icon1
PRE_TARGETDEPS += $$copy_icon1.target


copy_icon2.target = $$OUT_PWD/root/name.png
copy_icon2.depends = $$_PRO_FILE_PWD_/name.png
copy_icon2.commands = $(COPY_FILE) \"$$replace(copy_icon2.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_icon2.target, /, $$QMAKE_DIR_SEP)\"
QMAKE_EXTRA_TARGETS += copy_icon2
PRE_TARGETDEPS += $$copy_icon2.target

copy_icon3.target = $$OUT_PWD/root/kids_focus.png
copy_icon3.depends = $$_PRO_FILE_PWD_/kids_focus.png
copy_icon3.commands = $(COPY_FILE) \"$$replace(copy_icon3.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_icon3.target, /, $$QMAKE_DIR_SEP)\"
QMAKE_EXTRA_TARGETS += copy_icon3
PRE_TARGETDEPS += $$copy_icon3.target

copy_icon4.target = $$OUT_PWD/root/kids_normal.png
copy_icon4.depends = $$_PRO_FILE_PWD_/kids_normal.png
copy_icon4.commands = $(COPY_FILE) \"$$replace(copy_icon4.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_icon4.target, /, $$QMAKE_DIR_SEP)\"
QMAKE_EXTRA_TARGETS += copy_icon4
PRE_TARGETDEPS += $$copy_icon4.target

copy_icon5.target = $$OUT_PWD/root/focus_dis.png
copy_icon5.depends = $$_PRO_FILE_PWD_/focus_dis.png
copy_icon5.commands = $(COPY_FILE) \"$$replace(copy_icon5.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_icon5.target, /, $$QMAKE_DIR_SEP)\"
QMAKE_EXTRA_TARGETS += copy_icon5
PRE_TARGETDEPS += $$copy_icon5.target

copy_config.target = $$OUT_PWD/root/config.xml
copy_config.depends = $$_PRO_FILE_PWD_/config.xml
copy_config.commands = $(COPY_FILE) \"$$replace(copy_config.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_config.target, /, $$QMAKE_DIR_SEP)\"
QMAKE_EXTRA_TARGETS += copy_config
PRE_TARGETDEPS += $$copy_config.target



wgt.target = package
wgt.commands = wgtpkg-pack -f -o mirroring.wgt root

QMAKE_EXTRA_TARGETS += wgt

deploy.files = mirroring.wgt
deploy.path = /usr/AGL/apps/autoinstall
INSTALLS += deploy
