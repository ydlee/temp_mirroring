TEMPLATE = subdirs
SUBDIRS = app package
package.depends += app

lupdate_only {
    SOURCES =  app/*.qml
}

TRANSLATIONS  = app/lang_ko.ts
