## 2019.04.17
어플리케이션 빌드 방법
```
$ git clone https://gitlab.com/Hobbangman/mx_rse_mirroring_gui.git
$ cd mx_rse_mirroring_gui
$ source ${sdk_dir}/environment-setup-cortexa9hf-neon-agl-linux-gnueabi
$ qmake mirroring.pro -spec linux-oe-g++ CONFIG+=debug CONFIG+=qml_debug
$ make
```

# 2019.03.26
- 키즈용 아이콘 이미지 추가
