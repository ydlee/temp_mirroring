///////////////////////////////////////////////////////////
//  CBottombar.h
//  Implementation of the Class CBottombar
//  Created on:      28-7-2020 오후 2:47:34
//  Original author: user
///////////////////////////////////////////////////////////

#ifndef __CBOTTOMBAR_H__
#define __CBOTTOMBAR_H__


#include "sharddata.h"
#include <QtQml/QQmlContext>



class CBottombar
{

public:
	CBottombar();
    virtual ~CBottombar();

public:

    virtual void showBottobar()=0;
};

#endif // !defined(EA_203F5C62_5A25_4148_B6BE_835DE56AC2D5__INCLUDED_)
