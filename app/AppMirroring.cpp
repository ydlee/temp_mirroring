///////////////////////////////////////////////////////////
//  AppImagePlayer.cpp
//  Implementation of the Class AppImagePlayer
//  Created on:      28-7-2020 오후 2:43:48
//  Original author: user
///////////////////////////////////////////////////////////
#include <QTextCodec>
#include <QCommandLineParser>
#include "AppMirroring.h"


AppMirroring::AppMirroring(int argc, char** argv,QString apptitle)
 : QGuiApplication(argc, argv)
 , m_appTitle(apptitle)
 , CFormCtrl()
{
    //TODO;
    init();
}

AppMirroring::~AppMirroring(){
    //TODO;
}

void AppMirroring::init(){

     QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
     QStringList ws = parserCommandLine();
     if (ws.length() == 2) {
        int port = ws.takeFirst().toInt();
        QString secret = ws.takeFirst();
        m_ws.nPort = port;
        m_ws.qstrSecret = secret;
    }


    m_setup = new setupservice(this, true);

    CFormCtrl::setTitle(m_appTitle);
    CFormCtrl::socketInit(m_ws.nPort, m_ws.qstrSecret.toStdString().c_str());
    CFormCtrl::QmlInit("main.qml");
}

//start
void AppMirroring::showqml(){

    qDebug() << "showqml()";
    QObject*  root =  CFormCtrl::getViewPtr()->rootObjects().first();
    QQuickWindow* window = qobject_cast<QQuickWindow *>(root);
    window->show();

}

QStringList AppMirroring::parserCommandLine(){

    QStringList arrylist;
    QCommandLineParser parser;
    parser.addPositionalArgument("port", qGuiApp->translate("main", "port for binding"));
    parser.addPositionalArgument("secret", qGuiApp->translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(*qGuiApp);
    if(parser.positionalArguments().length() < 2){
        arrylist.append("0");
        arrylist.append("");
    }else{
        arrylist = parser.positionalArguments();
    }
    return arrylist;
}


void AppMirroring::setIsState(int value)
{
    qDebug() << __FUNCTION__;
//    if(m_isState == value)
//        return;


//    m_isState = value;

//    emit isStateChanged();

}

