/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2017 Konsulko Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtCore/QCommandLineParser>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QStandardPaths>
#include <QtCore/QUrlQuery>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQml/qqml.h>
#include <QQuickWindow>
#include <libhomescreen.hpp>
#include <qlibwindowmanager.h>
#include <QJsonObject>
#include "backend.h"
#include "setup/setupservice.h"
#include "fileio.h"
#include <qtappfw/messageengine.h>
#include <qtappfw/bottombar.h>
#include <qtappfw/homescreen.h>
#include <qtappfw/soundmanager.h>

int main(int argc, char *argv[])
{
    QString myname = QString("Mirroring");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();

    BackEnd *backend = new BackEnd(&app);

    engine.rootContext()->setContextProperty("beep",  backend);
    engine.rootContext()->setContextProperty("theme", backend->get_theme());
    engine.rootContext()->setContextProperty("servicesetup", backend->get_setupservice());
    engine.rootContext()->setContextProperty("ts", backend->get_setupservice());
    engine.rootContext()->setContextProperty("backend", backend);

    QCommandLineParser parser;
    parser.addPositionalArgument("port", app.translate("main", "port for binding"));
    parser.addPositionalArgument("secret", app.translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);
    QStringList positionalArguments = parser.positionalArguments();

    if (positionalArguments.length() == 2) {
        int port = positionalArguments.takeFirst().toInt();
        QString secret = positionalArguments.takeFirst();
        QUrl bindingAddress;
        bindingAddress.setScheme(QStringLiteral("ws"));
        bindingAddress.setHost(QStringLiteral("127.0.0.1"));
        bindingAddress.setPort(port);
        bindingAddress.setPath(QStringLiteral("/api"));
        QUrlQuery query;
        query.addQueryItem(QStringLiteral("token"), secret);
        bindingAddress.setQuery(query);
        context->setContextProperty(QStringLiteral("bindingAddress"), bindingAddress);
        std::string token = secret.toStdString();
        LibHomeScreen* hs = new LibHomeScreen();
        QLibWindowmanager* qwm = new QLibWindowmanager();

        // 초기화 코드, 두 객체를 초기화 시켜주어야 한다, bindingAddress는 QUrl 객체
        MessageEngine *messageEngine = new MessageEngine(bindingAddress, NULL);
        SoundManager  *soundmanager  = new SoundManager(messageEngine);
        ModeManager   *modemanager   = new ModeManager(messageEngine);
        HomeScreen    *homescreen    = new HomeScreen(messageEngine);
        BottomBar     *bottombar     = new BottomBar(messageEngine);

        backend->set_websocket(bindingAddress.toString());

        // WindowManager
        if(qwm->init(port,secret) != 0){
            exit(EXIT_FAILURE);
        }
        // Request a surface as described in layers.json windowmanager’s file
        if (qwm->requestSurface(myname) != 0) {
            exit(EXIT_FAILURE);
        }
        // Create an event callback against an event type. Here a lambda is called when SyncDraw event occurs
        qwm->set_event_handler(QLibWindowmanager::Event_SyncDraw, [qwm, myname ,backend ](json_object *object) {
            fprintf(stderr, "Surface got syncDraw!\n");
            backend->reInit(myname);
            qwm->endDraw(myname);
        });


        qwm->set_event_handler(QLibWindowmanager::Event_Invisible, [qwm, myname, backend](json_object *object) {
            QString label = QString(json_object_get_string(	json_object_object_get(object, "drawing_name") ));
            //qDebug() << "[SY] Event_Invisible" << label;
            if(label == "Mirroring")
            {
                //bIsBackgroud = true;
                //qDebug() << "bIsBackgroud : " << bIsBackgroud;

                //Mode change
                backend->setModeInVisible();
            }
        });

        qwm->set_event_handler(QLibWindowmanager::Event_Visible, [qwm, myname, backend](json_object *object) {
            QString label = QString(json_object_get_string(	json_object_object_get(object, "drawing_name") ));

            if(label == "Mirroring")
            {
                /*
                qDebug() << "bIsBackgroud : " << bIsBackgroud;
                if(bIsBackgroud)
                {
                    backend->CheckWIFIonAirplay();
                }
                bIsBackgroud = false;
                */

                //Mode change
                backend->setModeVisible();
            }


            //Touch enable
            /*
            if (tmp.compare("PhotoViewer_visible") == 0) {
                udsManager->showScreen();
            }
            */

        });


        qwm->set_event_handler(QLibWindowmanager::Event_Active, [qwm, myname, backend](json_object *object){
            QString label = QString(json_object_get_string( json_object_object_get(object, "drawing_name") ));
            QString myname = QCoreApplication::applicationName();

            //qDebug() << "[SY] Event_Active" << label;

            if(label == "Mirroring")
            {
                // 자신이 Activate 되면 미디어를 실행하기 위한 쉘 스크립트 실행
                //qDebug() << "QLibWindowmanager::Event_Active : mirroring";
                //system(QString("/var/lib/lxc/android/start_" + myname + ".sh &").toStdString().c_str());
                //system("lxc-attach -v PATH=/system/bin -v ANDROID_DATA=/data -n android -- /system/bin/sh -c \"service call audio 3 i32 3 i32 15 i32 0\" &");

                //Mode change
                backend->setModeActive();
            }
        });

        qwm->set_event_handler(QLibWindowmanager::Event_Inactive, [qwm, myname, backend](json_object *object){
            QString label = QString(json_object_get_string( json_object_object_get(object, "drawing_name") ));
            QString myname = QCoreApplication::applicationName();

            //qDebug() << "[SY] Event_Inactive" << label;

            if(label == "Mirroring")
            {
                // 자신이 Deactivate 되면 미디어를 종료하기 위한 쉘 스크립트 실행
                //qDebug() << "QLibWindowmanager::Event_Inactive : mirroring";
                //system("killall -9 com.twobeone.motrex20 &");

                backend->setModeInActive();
            }

        });

        // HomeScreen
        hs->init(port, token.c_str());
        // Set the event handler for Event_TapShortcut which will activate the surface for windowmanager
        hs->set_event_handler(LibHomeScreen::Event_TapShortcut, [qwm, myname](json_object *object){
            json_object *appnameJ = nullptr;
            if(json_object_object_get_ex(object, "application_name", &appnameJ))
            {
                const char *appname = json_object_get_string(appnameJ);
                if(myname == appname)
                {
                    qDebug("Surface %s got tapShortcut\n", appname);

                    qwm->activateSurface(myname);
                }
            }
        });


        hs->set_event_handler(LibHomeScreen::Event_OnScreenReply, [myname, backend](json_object *object){
            json_object *response = nullptr;
            if(json_object_object_get_ex(object, "reply_message", &response)) {
                const char *message = json_object_get_string(response);
                qDebug() << "LibHomeScreen::Event_OnScreenReply : " << message;
                if(strcmp(message, "mirroring:no") == 0)
                {
                    // 처리
                    //qDebug() << "LibHomeScreen::Event_OnScreenReply : " << message;

                    backend->call_run_Engine();
                }
            }
        });


        backend->setWM(qwm);
        backend->setHS(hs);
        backend->setMM(modemanager);
        backend->setSM(soundmanager);

        // Declaring your C++ class to the QML system
        qmlRegisterType<FileIO, 1>("FileIO", 1, 0, "FileIO");

        // for Bottombar
        engine.rootContext()->setContextProperty("homescreen", homescreen);
        engine.rootContext()->setContextProperty("bottombar", bottombar);
        engine.rootContext()->setContextProperty("windowmanager", qwm);

        engine.load(QUrl(QStringLiteral("qrc:/Mirroring.qml")));
    }

    return app.exec();
}

