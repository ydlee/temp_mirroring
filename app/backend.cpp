#include "backend.h"
#include "DAUtilities.h"
#include <QThread>
#include <QFile>


BackEnd::BackEnd(QGuiApplication *app)
{

    QThread *thread = new QThread;

    //display default setting
    //displaySetting();

    m_pIpcSocket = new DAIpcSocket();
    m_pIpcSocket->initIpcSocket();
    connect(m_pIpcSocket, SIGNAL(receiveMessage(QString)), this, SLOT(ExtCmdProcedure(QString)));


    m_isSelectMode = 0;
    m_isControl = true;
    m_isState = NOTCONNECT;
    m_isBG = NOTBACKGROUND;
    m_isModeState = MODE_NONE;
    m_showPopup = false;

    m_process = new QProcess();

    connect(m_process, SIGNAL(readyReadStandardError()), this, SLOT(readyReadStandardError()));
    connect(m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(readyReadStandardOutput()));
    connect(m_process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(finished(int,QProcess::ExitStatus)));

    connect(this, SIGNAL(isStateChanged()), this, SLOT(ExtProcess_State()));
    connect(this, SIGNAL(isBGChanged()), this, SLOT(ExtProcess_State()));
    connect(this, SIGNAL(isPhoneNameChanged()), this, SLOT(ExtProcess_State()));
    connect(this, SIGNAL(signal_run_Engine()), this, SLOT(slot_run_Engine()));

    m_pIpcSocket->start();

    m_setup = new setupservice(app, true);

    m_setup->setTargets(QJsonArray().fromStringList(QStringList() << "mirr"));

    connect(thread, SIGNAL(started()), m_pIpcSocket, SLOT(run()));
    connect(m_pIpcSocket, SIGNAL(onReceive()),this,SLOT(onReceive()));
    m_pIpcSocket->moveToThread(thread);
    thread->start();
}

BackEnd::~BackEnd()
{

}

void BackEnd::reInit(QString name)
{
    QString tmp = setJsonValue(nullptr,"SEND_UDS_PATH");

    tmp = setJsonValue(tmp,"NAME",name);

    tmp = setJsonValue(tmp,"PATH",SOCK_LOCALFILE);

    m_pIpcSocket->sendMessage(tmp);
}

int BackEnd::getIsSelect() const
{
    return m_isSelectMode;
}
void BackEnd::setIsSelectMode(int value)
{
    if(m_isSelectMode != value)
        m_isSelectMode = value;

    emit isSelectModeChanged();
}

int BackEnd::getIsState() const
{
    qDebug() << __FUNCTION__;
    return m_isState;
}

void BackEnd::setIsState(int value)
{
    qDebug() << __FUNCTION__;
    if(m_isState == value)
        return;


    m_isState = value;

    emit isStateChanged();

}

bool BackEnd::getIsBG() const
{
    qDebug() << "[SY]" << __FUNCTION__<<"m_isBG : " << m_isBG;
    return m_isBG;
}

void BackEnd::setIsBG(int value)
{
    //if(m_isBG == value)
    //    return;

    qDebug() << "[SY]" << __FUNCTION__<<"value : " << value;
    m_isBG = value;

    emit isBGChanged();
}


QString BackEnd::getPhoneName() const
{
    qDebug() << "[SY]" << __FUNCTION__<<"m_Phonename : " << m_Phonename;
    return m_Phonename;
}

void BackEnd::setPhoneName(QString value)
{
    //if(m_isBG == value)
    //    return;

    qDebug() << "[SY]" << __FUNCTION__<<"value : " << value;
    m_Phonename = value;


    emit isPhoneNameChanged();
}




Theme* BackEnd::get_theme()
{
    return m_setup->get_theme();
}

setupservice* BackEnd::get_setupservice()
{
    return m_setup;
}

void BackEnd::set_websocket(QString url)
{
    m_setup->setUrl(url);
}

void BackEnd::play()
{
    m_sm->play();
}


void BackEnd::ExtCmdProcedure(QString cmd)
{
    int result_getphonename;
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;
    if(cmd == "MIRASuccess")
    {
       //get Phone name
       result_getphonename = get_Phonename();
       if (result_getphonename)
       {
           setIsState(SHOW_PHONENAME_BEFORECONNECTED);
       }

       setIsBG(NOTBACKGROUND);
       //setIsState(CONNECTED);

    }
    else if(cmd == "AirSuccess")
    {
        setPhoneName("iPhone");
        setIsState(SHOW_PHONENAME_BEFORECONNECTED);

        setIsBG(NOTBACKGROUND);
        //setIsState(CONNECTED);
    }
    else if(cmd == "AirWifiEnd" || cmd == "AirConnectionEnd" || cmd == "MIRAWifiEnd" || cmd == "MIRAConnectionEnd")
    {
        system("killall kivicmain");
        system("killall gst-launch-1.0 &");


        //setIsControl(1);
        //setIsConnected(0);

        //setIsState(NOTCONNECT);
        emit openPopup();
    }

}

void BackEnd::ExtProcess_State()
{
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    if( m_isState == NOTCONNECT )
    {

    }
    else if(m_isState == CONNETTING)
    {

    }
    else if(m_isState == CONNECTED)
    {

    }
    else if(m_isState == GOHOME)
    {
        //alphablend off
        system("modetest -M nexell -w 18:alphablend:16");
        goHomelauncher();
    }

}

void BackEnd::readyReadStandardError()
{
    QByteArray arr;
    arr = m_process->readAllStandardError();
    qDebug() <<  "[Process]" << __FUNCTION__ << arr;
}
void BackEnd::readyReadStandardOutput()
{
    QByteArray arr;
    arr = m_process->readAllStandardOutput();
    qDebug() <<  "[Process]" << __FUNCTION__ << arr;
}
void BackEnd::finished(int exitCode, QProcess::ExitStatus exitStatus)
{
    qDebug() << "exitCode: " << exitCode  << ",exitStatus: " << exitStatus;
    QString sendCommand;
}

void BackEnd::onReceive(int value)
{
    qDebug() << __FUNCTION__;
}

int BackEnd::displaySetting()
{
    //defualt setting values
    /*
     * brightness : 0
     * contrast : 4
     * saturation : 64
     */

    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;
    system("modetest -M nexell -w 27:brightness:0");
    system("modetest -M nexell -w 27:contrast:4");
    system("modetest -M nexell -w 27:saturation:64");
    return 0;
}

//wifi check only airplay
double BackEnd::CheckWIFIrssi()
{
    qDebug() << __FUNCTION__;
    double level;

    QJsonObject jsonObj = m_setup->status();
    //qDebug() << "jsonObj :" << jsonObj;
    QJsonValue jsonValuewifi = jsonObj.value(QString("wifi"));
    //qDebug() << "jsonObjwifi :" << jsonValuewifi;
    QJsonObject jsonObjWifi = jsonValuewifi.toObject();
    //qDebug() << "jsonObjWifi :" << jsonObjWifi;
    QJsonValue jsonValueLevel = jsonObjWifi.value((QString("level")));
    //qDebug() << "jsonValueLevel :" << jsonValueLevel;
    level = jsonValueLevel.toDouble();
    qDebug() << "level :" << level;

    return level;

}




//wifi check only airplay
int BackEnd::CheckWIFIonAirplay()
{
    qDebug() << __FUNCTION__;
    double level;


    //only airplay
    if(m_isSelectMode == IPHONE)
    {

        level = CheckWIFIrssi();

        if(level == 0)
        {
            setIsState(NOTCONNECT);

            system("killall kivicmain &");
        }
    }


    return 0;
}

//Visible
int BackEnd::setModeVisible()
{
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    m_setup->updateSettings();

    setIsBG(NOTBACKGROUND);

    m_isModeState = MODE_VISIBLE;
    if( m_isState == CONNECTED )
    {
        setIsBG(NOTBACKGROUND);
        //alphablend
        system("modetest -M nexell -w 18:alphablend:12");
        qDebug() << "[SY] m_isState == CONNECTED";

        if(m_isSelectMode == IPHONE)
        {
            CheckWIFIonAirplay();
        }
    }
}

//InVisible
int BackEnd::setModeInVisible()
{
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    m_isModeState = MODE_INVISIBLE;

    setIsBG(BACKGROUND);

    if( m_isState == CONNECTED )
    {
        setIsBG(BACKGROUND);
        //alphablend off
        system("modetest -M nexell -w 18:alphablend:16");
        qDebug() << "[SY] m_isState == CONNECTED";
    }
    else if ( m_isState == CONNETTING )
    {
        system("killall kivicmain &");
        QThread::msleep(100);
        system("killall gst-launch-1.0 &");
        QThread::msleep(100);
        if(m_isSelectMode == ANDROUD)
        {
            system("killall wpa_cli");
            QThread::msleep(300);
            system("killall wpa_supplicant");
            QThread::msleep(300);
            system("killall udhcpd");
            QThread::msleep(300);
            system("systemctl restart connman &");
            QThread::msleep(300);
        }
        setIsState(NOTCONNECT);
    }
}

//Active
int BackEnd::setModeActive()
{
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

}


//InActive
int BackEnd::setModeInActive()
{
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    if( m_isState == CONNECTED )
    {
        setIsBG(BACKGROUND);
        system("killall kivicmain &");
        QThread::msleep(100);
        system("killall gst-launch-1.0 &");
        QThread::msleep(100);
        if(m_isSelectMode == ANDROUD)
        {
            system("killall wpa_cli");
            QThread::msleep(300);
            system("killall wpa_supplicant");
            QThread::msleep(300);
            system("killall udhcpd");
            QThread::msleep(300);
            system("systemctl restart connman &");
            QThread::msleep(300);
        }
        setIsState(NOTCONNECT);
    }
    else if(m_isState == CONNETTING)
    {
        system("killall kivicmain &");
        QThread::msleep(100);
        system("killall gst-launch-1.0 &");
        QThread::msleep(100);
        if(m_isSelectMode == ANDROUD)
        {
            system("killall wpa_cli");
            QThread::msleep(300);
            system("killall wpa_supplicant");
            QThread::msleep(300);
            system("killall udhcpd");
            QThread::msleep(300);
            system("systemctl restart connman &");
            QThread::msleep(300);
        }
        setIsState(NOTCONNECT);
    }
//    else if(m_isState == NOTCONNECT)
//    {
//    }

}

void BackEnd::slotBackButtonClicked()
{
    QString myname = QString("Mirroring");
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    if(m_showPopup) //backbutton disable 될 경우 삭제
    {        
        closePopup();
        return;
    }

    if(m_isModeState != MODE_VISIBLE)
    {
        qDebug() << "skip backbutton";
        return;
    }


    if( m_isState == NOTCONNECT )
    {
        //ToDo - code here system call

        //kill from homescreen
        //QString sendMsg = setJsonValue(nullptr,"IS_LAYER_TOP");
        //sendMsg = setJsonValue(sendMsg,"FUNCTION","1");
        //m_pIpcSocket->sendMessage(sendMsg);

        //m_qwm->deactivateSurface(myname);
        m_mm->exitApplication(myname);

    }
    else if(m_isState == CONNETTING)
    {
        system("killall kivicmain &");
        QThread::msleep(100);
        system("killall gst-launch-1.0 &");
        QThread::msleep(100);
        if(m_isSelectMode == ANDROUD)
        {
            system("killall wpa_cli");
            QThread::msleep(300);
            system("killall wpa_supplicant");
            QThread::msleep(300);
            system("killall udhcpd");
            QThread::msleep(300);
            system("systemctl restart connman &");
            QThread::msleep(300);
        }
        setIsState(NOTCONNECT);
    }
    else if(m_isState == CONNECTED)
    {
        system("killall kivicmain &");
        QThread::msleep(100);
        system("killall gst-launch-1.0 &");
        QThread::msleep(100);
        if(m_isSelectMode == ANDROUD)
        {
            system("killall wpa_cli");
            QThread::msleep(300);
            system("killall wpa_supplicant");
            QThread::msleep(300);
            system("killall udhcpd");
            QThread::msleep(300);
            system("systemctl restart connman &");
            QThread::msleep(300);
        }
        setIsState(NOTCONNECT);

    }
    else if(m_isState == SHOW_PHONENAME_BEFORECONNECTED)
    {
        system("killall kivicmain &");
        QThread::msleep(100);
        system("killall gst-launch-1.0 &");
        QThread::msleep(100);
        if(m_isSelectMode == ANDROUD)
        {
            system("killall wpa_cli");
            QThread::msleep(300);
            system("killall wpa_supplicant");
            QThread::msleep(300);
            system("killall udhcpd");
            QThread::msleep(300);
            system("systemctl restart connman &");
            QThread::msleep(300);
        }
        setIsState(NOTCONNECT);
    }
}

int BackEnd::setWM(QLibWindowmanager *qwm)
{
    m_qwm = qwm;
    return 0;
}

int BackEnd::setHS(LibHomeScreen* hs)
{
    m_hs = hs;
    return 0;
}

int BackEnd::setMM(ModeManager* mm)
{
    m_mm = mm;
    return 0;
}

int BackEnd::setSM(SoundManager* sm)
{
    m_sm = sm;
    return 0;
}


//Get Event_OnScreenReply "no"
int BackEnd::run_Engine()
{
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    if(m_process->state() != QProcess::NotRunning)
    {
        m_process->kill();
        m_process->waitForFinished();
    }

    QString command;

    switch (m_isSelectMode)
    {
        //Android
        case 0:
            command = "/home/root/kivic/wfdp2p.sh";

            break;
        //iPhone
        case 1:
            command = "/usr/bin/kivicmain";

            break;
        default:
            break;
    }

    qDebug() << __FUNCTION__ << command;

    m_process->start(command);

    QJsonObject jsonObj;
    jsonObj.insert("selectedMode",QJsonValue::fromVariant(m_isSelectMode));
    m_setup->set_target("mirr",jsonObj);
    qDebug() << "**************";

    setIsState(CONNETTING);


    return 0;
}


int BackEnd::get_Phonename()
{
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    QProcess *process = new QProcess();
    int nResult = 0;

//    if(process->state() != QProcess::NotRunning)
//    {
//        process->kill();
//        process->waitForFinished();
//    }

    QString command = "/usr/sbin/wpa_cli p2p_peers";

    process->start(command);
    process->waitForFinished(-1);

    //QByteArray arr;
    //arr = process->readAllStandardOutput();
    //qDebug() << "************** " << arr;
    process->setReadChannel(QProcess::StandardOutput);
    QByteArray arr;
    while (process->canReadLine())
    {
       arr = process->readLine();

    }
    //qDebug() << "====================== ";
    //qDebug() << arr;
    //qDebug() << "====================== ";

    QString mac = QString::fromUtf8(arr);
    mac.remove(mac.size()-1, 1);

    //qDebug() << "====================== ";
    //qDebug() << mac;
    //qDebug() << "====================== ";

    QString commnad2 = "/usr/sbin/wpa_cli p2p_peer";
    QString command2full = commnad2 + " " + mac;
    //qDebug() << "**************";
    //qDebug() << command2full;
    //qDebug() << "**************";
    QProcess *process2 = new QProcess();
//    if(process2->state() != QProcess::NotRunning)
//    {
//        process2->kill();
//        process2->waitForFinished();
//    }

    process2->start(command2full);
    process2->waitForFinished(-1);

    process2->setReadChannel(QProcess::StandardOutput);
    QByteArray arr2;
    QString phone_name;
    while (process2->canReadLine())
    {
       arr2 = process2->readLine();
       phone_name = QString::fromUtf8(arr2);
       bool ret = phone_name.contains("device_name", Qt::CaseInsensitive);
       if(ret)
       {
           QStringList list = phone_name.split("=");
           qDebug() << "**************";
           qDebug() << list[1];
           qDebug() << "**************";
           //m_Phonename = list[1];
           setPhoneName(list[1]);
           nResult = 1;
           break;
       }

    }

    return nResult;

}

void BackEnd::goHomelauncher()
{
    QString myname = QString("Mirroring");
    m_qwm->deactivateSurface(myname);

    system("killall kivicmain &");
    QThread::msleep(100);
    system("killall gst-launch-1.0 &");
    QThread::msleep(100);
    if(m_isSelectMode == ANDROUD)
    {
        system("killall wpa_cli");
	    QThread::msleep(300);
        system("killall wpa_supplicant");
        QThread::msleep(300);
        system("killall udhcpd");
        QThread::msleep(300);
        system("systemctl restart connman &");
        QThread::msleep(300);
    }
    setIsState(NOTCONNECT);
}


int BackEnd::call_run_Engine()
{
    emit signal_run_Engine();
}

void BackEnd::slot_run_Engine()
{
    run_Engine();
}

void BackEnd::onScreenReply(QString value)
{
    m_hs->onScreenReply(value.toUtf8());
}

bool BackEnd::showPopup()
{
    return m_showPopup;
}

void BackEnd::closePopup()
{
    if(m_showPopup)
    {
        emit popupBackButtonClicked();
    }        
}

void BackEnd::setShowPopup(bool bShowPopup)
{
    m_showPopup = bShowPopup;
    emit showPopupChanged();
}
