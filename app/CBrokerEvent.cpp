///////////////////////////////////////////////////////////
//  CBrokerEvent.cpp
//  Implementation of the Class CBrokerEvent
//  Created on:      28-7-2020 오후 11:09:46
//  Original author: user
///////////////////////////////////////////////////////////

#include "CBrokerEvent.h"
#include "CIPCService.h"
#include <QtCore/QUrlQuery>
#include <qdebug.h>
#include <QCoreApplication>
#include <CFormCtrl.h>
//#include "setup/setupservice.h"

CBrokerEvent::CBrokerEvent(QObject *parent)
    : QObject(parent)
    , m_myname("")
    , CIPCService()
{
    init();
}


CBrokerEvent::~CBrokerEvent(){
 //TODO
#if LINUX_X86_ENABLE
    if(m_webSocket.state() > 0)
        m_webSocket.close();
#else
    delete m_pIpcSocket;
#endif


}

void CBrokerEvent::init(){
    //TODO
#if LINUX_X86_ENABLE
    m_webSocket.close();
#endif
    QMutexLocker lock(&m_dataMutex); // locks mutex

#if 0
    m_setup = new setupservice(app, true);
#endif

     QThread *thread = new QThread;

     m_pIpcSocket = new DAIpcSocket();

     m_pIpcSocket->start();

    connect(thread, SIGNAL(started()), m_pIpcSocket, SLOT(run()));
    connect(m_pIpcSocket, SIGNAL(onReceive()),this,SLOT(onReceive()));
    m_pIpcSocket->moveToThread(thread);
    thread->start();

}

void CBrokerEvent::onReceive(int value)
{
    qDebug() << __FUNCTION__;
}

int CBrokerEvent::webSocktInit(QStringList positionalArguments)
{
    int port = positionalArguments.takeFirst().toInt();
    QString secret = positionalArguments.takeFirst();
    return webSocktInit(port,secret.toStdString().c_str());
}

int  CBrokerEvent::webSocktInit(int port, const char *chSecret)
{

     CIPCService::m_url = CIPCService::getUrlAddressInit(port,chSecret);

#if LINUX_X86_ENABLE

    if (CIPCService::getUrlAddressInit(m_webSocket, m_url) == true) {
       connect(&m_webSocket, &QWebSocket::connected, this, &CBrokerEvent::onConnected);
    // connect(&m_webSocket, &QWebSocket::disconnected, this, &CBrokerEvent::closed);
    }
#endif

    return 0;
}

int CBrokerEvent::messageEngineInit(QUrl& url)
{
      m_pMessageEngine = new MessageEngine(url, NULL);
      if(nullptr == m_pMessageEngine) return 1;

      m_pSoundmanager  = new SoundManager(m_pMessageEngine);
      if(nullptr ==  m_pSoundmanager ) return 2;

      m_pModemanager   = new ModeManager(m_pMessageEngine);
      if(nullptr ==  m_pModemanager ) return 3;

      m_pHomescreen    = new HomeScreen(m_pMessageEngine);
      if(nullptr ==  m_pHomescreen ) return 3;

      m_pBottombar     = new BottomBar(m_pMessageEngine);
      if(nullptr ==  m_pBottombar ) return 4;

      set_websocket(url.toString());

    return 0;
}

#if LINUX_X86_ENABLE
void CBrokerEvent::onConnected()
{
    qDebug() << "onConnected...";

    qDebug() << "WebSocket connected";
    connect(&m_webSocket, &QWebSocket::textMessageReceived,this, &CBrokerEvent::onTextMessageReceived);
    m_webSocket.sendTextMessage(QStringLiteral("Hello, world!"));
}

void CBrokerEvent::onTextMessageReceived(QString message)
{
    qDebug() << "onTextMessageReceived...";
    qDebug() << "Message received:" << message;
//    m_webSocket.close();
    // ??  callBack function
//    void CFormCtrl::setnum(const int &gn );

//    connect(&signal , SIGNAL(CallbackSignal(int , QString)) , this , SLOT(Handler(int , QString)));

    MessageSend();

}

#endif


void CBrokerEvent::MessageSend()
{


#if DEBUG_TEST_TEMP_ENABLE

     CFormCtrl* ctl = (CFormCtrl*)this;
     QString msg ="Call Test";
//     bool val = QMetaObject::invokeMethod(ctl, "func2", Q_RETURN_ARG(QString, msg));
//     QMetaObject::invokeMethod(ctl, "func2", Qt::DirectConnection, Q_ARG(QString, msg));

//     qDebug() << "call CBrokerEvent::func2 returns" << val;
#else
    qDebug() << "call-1  m_dataMutex.lock" ;
    m_dataMutex.lock();
    qDebug() << "call-2  m_dataMutex.lock" ;
    CFormCtrl* ctl = (CFormCtrl*)this;

#if LINUX_X86_ENABLE
    ctl->printState(1);
#endif

    m_dataMutex.unlock();

#endif

}

void CBrokerEvent::set_websocket(QString url)
{
    CFormCtrl* ctl = ((CFormCtrl*)this);
    ctl->m_setup->setUrl(url);
}


