///////////////////////////////////////////////////////////
//  CIPCService.h
//  Implementation of the Class CIPCService
//  Created on:      30-7-2020 오후 12:20:43
//  Original author: user
///////////////////////////////////////////////////////////
#ifndef __CIPCSERVICE_H__
#define __CIPCSERVICE_H__
#include "sharddata.h"

#include "CLogger.h"
#include <QtCore/QUrlQuery>
#include <QtWebSockets/QWebSocket>




class CIPCService
{

public:
	CIPCService();
	virtual ~CIPCService();

private:
    void init();

protected:
    QUrl m_url;
#if LINUX_X86_ENABLE
    QWebSocket m_webSocket;
#endif

public:
    QUrl getUrlAddressInit(int port, const char *chSecret);


#if LINUX_X86_ENABLE
    int echoClient(const QUrl &url);
    int openWebClient(QWebSocket& webSocket,const QUrl &url);
    bool getUrlAddressInit(QWebSocket& webSocket,QUrl& url);
    bool getUrlAddressInit(QWebSocket& webSocket,int port, const char *chSecret);
#endif

};

#endif // !defined(EA_5A46764E_D28C_4d01_BBF6_A7A1E1701148__INCLUDED_)
