#ifndef THEME_H
#define THEME_H

//#include "resource.h"
#include <QObject>
#include <QString>
#include <QJsonArray>
#include <QJsonObject>

class Theme: public QObject
{

    Q_OBJECT

    Q_PROPERTY(QJsonObject image READ image NOTIFY imageChanged)
    Q_PROPERTY(QString tr READ tr NOTIFY trChanged)

public:
    Theme();
    ~Theme();



public:

    QJsonObject image();
    void setTheme(QString);

    QString tr();
    void setTr();

private:
    QJsonObject *_image;
    QString _tr = QString("%1");


signals:
    void imageChanged();
    void trChanged();


};

#endif // THEME_H
