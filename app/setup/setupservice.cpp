#include "setupservice.h"

#include <QObject>
#include <QResource>
#include <QWebSocket>
#include <QUrl>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QtNetwork>

setupservice::setupservice(QGuiApplication *app)
{
    connect(this,&QWebSocket::connected,this,&setupservice::serviceConnected);
    connect(this,&QWebSocket::disconnected,this,&setupservice::serviceDisconnected);
    connect(this,&QWebSocket::stateChanged,this,&setupservice::serviceStateChanged);
    connect(this,&QWebSocket::textMessageReceived,this,&setupservice::serviceHandler);
    connect(this,&setupservice::urlChanged,this,&setupservice::serviceUrlChanged);
    _app = app;
    pre_read_target("common");
    theme_name    = _conf.value("common").toObject().value("theme").toString();
    QString _lang = _conf.value("common").toObject().value("language").toString();
    update_theme(theme_name);
    update_language(_lang);
}

setupservice::~setupservice()
{

}



void setupservice::pre_read_target(QString target){
    QFile file;
    QString content;
    file.setFileName("/home/root/" + target + ".json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    content  = file.readAll();
    file.close();

    QJsonDocument doc = QJsonDocument::fromJson(content.toUtf8());
    _conf.insert(target,doc.object());
    //qDebug() << "pre_read_data" << _conf;
    setConf(_conf);
}

void setupservice::update_theme(QString str){
    qDebug() << __FUNCTION__ << str;
}

void setupservice::update_language(QString str){
    if(str.compare(QString("kr"))== 0){
        qDebug() << "remove translator" << _app->removeTranslator(translator);
        qDebug() << "load translator" << translator->load(":/lang_ko.qm");
        _app->installTranslator(translator);
        emit updateLanguage();
    }
    else {
        qDebug() << "remove translator" << _app->removeTranslator(translator);
        emit updateLanguage();
    }
}

QString setupservice::empty()
{
    return "";
}

void setupservice::set_app(QGuiApplication *app){
    _app = app;
}

Theme* setupservice::get_theme(){
    return _theme;
}

void setupservice::get_common(){
    QJsonObject obj;
    command(QString("get_common"),obj);
}

void setupservice::reset_common(){
    QJsonObject obj;
    command(QString("reset_common"),obj);
}


void setupservice::set_common(QJsonObject conf){
    QJsonObject obj;
    obj.insert(QString("data"),QJsonValue(conf));
    command(QString("set_common"),obj);
}

void setupservice::set_common_property(QString key,QString val){
//    QJsonObject common_obj = _conf.value("common").toObject();
    QJsonObject obj;
    obj.insert("key",QJsonValue(key));
    obj.insert("val",QJsonValue(val));
//    common_obj.insert(key,val);
    command(QString("set_common_property"),obj);
}

void setupservice::get_target(QString target){
    QJsonObject obj;
    obj.insert("target",QJsonValue(target));
    command(QString("get"),obj);
}

void setupservice::set_target(QString target,QJsonObject targetObj){
    QJsonObject obj;
    obj.insert(QString("target"),QJsonValue(target));
    obj.insert(QString("data"),QJsonValue(targetObj));
    command(QString("set"),obj);
}

void setupservice::set_target_property(QString target,QString key,QString val){
    QJsonObject obj;
    obj.insert(QString("target"),QJsonValue(target));
    obj.insert(QString("key"),QJsonValue(key));
    obj.insert(QString("val"),QJsonValue(val));
    command(QString("set_property"),obj);
}


void setupservice::init_target(QString target){
    QJsonObject obj;
    obj.insert(QString("target"),QJsonValue(target));
    command(QString("init"),obj);
}

void setupservice::serviceUrlChanged(QString url){
    qDebug() << "url Chang and open socket";
    this->open(QUrl(url));
}


QString setupservice::url(){
    return _url;
}

QJsonObject setupservice::conf(){
    return _conf;
}

bool setupservice::autoget(){
    return _autoget;
}

QJsonArray setupservice::targets(){
    return QJsonArray().fromVariantList(_targets);
}


void setupservice::setUrl(QString url){
    _url = url;
    emit this->urlChanged(_url);
}

void setupservice::setConf(QJsonObject conf){
    _conf = conf;
    emit this->confChanged(_conf);
}

void setupservice::setTargets(QJsonArray conf){
    _targets = conf.toVariantList();
    emit this->confChanged(_conf);
}

void setupservice::setAutoget(bool ag){
    _autoget = ag;
    emit this->autogetChanged(_autoget);
}


void setupservice::serviceStateChanged(QAbstractSocket::SocketState state){
    qDebug() << "[setupservice]" << "state changed" << state;

    if(state == QAbstractSocket::SocketState::ClosingState){
        qDebug() << "[setupservice]" << "closing state" << closeReason();
    }
}

void setupservice::serviceConnected(){
    qDebug() << "[setupservice]" << "service connected";
    QJsonObject obj;
    obj.insert("value",QJsonValue().fromVariant(QString("common_updated")));
    command("subscribe",obj);
    get_common(); // 전체 설정 값 받아오기
    foreach(QVariant row,_targets){
        init_target(row.toString());
        get_target(row.toString());
    }

    retry = 0;
}


void setupservice::serviceDisconnected(){
    qDebug() << "[setupservice]" << "service disconnected";
    if(retry < max_retry){
        this->open(QUrl(_url));
        retry++;
    }else{
        emit dead();
    }
}

void setupservice::command(QString verb, QJsonObject &obj){
    WaitingCommand.append(verb);
    QString tmp = apiString;
    tmp.append("/");
    tmp.append(verb);

    QJsonArray data;
    data.append(QJsonValue::fromVariant(MsgEnum::CALL));
    data.append(QJsonValue::fromVariant(payloadLength));
    data.append(QJsonValue::fromVariant(tmp));
    if(obj.isEmpty()){
        data.append(QJsonValue::fromVariant("NONE"));
    }
    else{
        data.append(QJsonValue::fromVariant(obj));
    }

    QJsonDocument saveDoc(data);

    //int result =
    this->sendTextMessage(saveDoc.toJson(QJsonDocument::Compact));
    //qDebug() << "[setupservice]" << saveDoc.toJson(QJsonDocument::Compact) << "commad result" << result;
}


void setupservice::serviceHandler(QString message){
    QJsonDocument jsondoc = QJsonDocument::fromJson(message.toUtf8());

    if(!jsondoc.isArray())
    {
        qDebug() << "[setupservice]" << "message format not allow";
        return;
    }

    QJsonArray data = jsondoc.array();

    MsgEnum type = (MsgEnum) data.at(0).toInt();
    QJsonObject obj = data.at(2).toObject();

    QJsonObject new_targetObject;

    QJsonValue res;
    QJsonObject res_obj;


    QString target;
    QString event;
    switch (type) {

    case MsgEnum::RETOK:

        res = obj.value("response");
        res_obj = res.toObject();
        target = WaitingCommand.takeFirst();
        qDebug() << "[setupservice]" << target << "OK" << obj;

        if(QString::compare(target,QString("get")) == 0){
            _conf.insert(res_obj.value("target").toString(),res_obj.value("data").toObject());
            setConf(_conf);
            emit confItemChanged(res_obj.value("target").toString());
            if(res_obj.value("target").toString().compare(QString("common")) == 0){
                QString _theme_name = _conf.value("common").toObject().value("theme").toString();
                if(QString::compare(_theme_name,theme_name) != 0){
                    theme_name = _theme_name;
                    update_theme(theme_name);
                }
                // 다국어 지원기능 테스트
                QString _lang = _conf.value("common").toObject().value("language").toString();
                update_language(_lang);
            }

            // language update test
        }else if(QString::compare(target,QString("get_common")) == 0){
            _conf.insert(res_obj.value("target").toString(),res_obj.value("data").toObject());
            setConf(_conf);
            emit confItemChanged(res_obj.value("target").toString());
        }else if(target.compare(QString("set")) == 0
//                 || target.compare(QString("set_common")) == 0
                 || target.compare(QString("set_property")) == 0
//                 || target.compare(QString("set_common_property")) == 0
//                 || target.compare(QString("reset_common")) == 0
                 ){

            if(_autoget){
                if(target.compare(QString("reset_common")) == 0){
                    res_obj.insert(QString("target"),QJsonValue().fromVariant(QString("common")));
                }
                get_target(res_obj.value("target").toString());
            }

        }

        break;

    case MsgEnum::RETERR:
        target = WaitingCommand.takeFirst();
        qDebug() << "[setupservice]" << target << "ERROR" << obj;
        break;

    case MsgEnum::EVENT:
//        target = WaitingCommand.takeFirst();
        qDebug() << "[setupservice]" << "EVENT" << obj;
        get_target(QString("common"));


        break;

    default:
        break;
    }
}
