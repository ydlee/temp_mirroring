///////////////////////////////////////////////////////////
//  AppImagePlayer.h
//  Implementation of the Class AppImagePlayer
//  Created on:      28-7-2020 오후 2:43:48
//  Original author: user
///////////////////////////////////////////////////////////

#ifndef __APPMIRRORING_H__
#define __APPMIRRORING_H__


#include <QGuiApplication>
#include "sharddata.h"
#include "CFormCtrl.h"

class AppMirroring : public QGuiApplication, public CFormCtrl
{

public:
   AppMirroring(int argc, char **argv, QString apptitle=QString(""));
   virtual ~AppMirroring();
private:
    
      QStringList parserCommandLine();
      stuWebSocket m_ws;
      QString m_appTitle;
      void init();

public:
   void showqml();
   void setIsState(int value);

};
#endif // !defined(EA_15926A87_E95D_4ad6_86BF_7FD0A442F81D__INCLUDED_)
