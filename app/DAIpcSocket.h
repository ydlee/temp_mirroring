#ifndef DAIPCSOCKET_H
#define DAIPCSOCKET_H

#include <QSocketNotifier>
#include <QObject>
#include <QThread>
#include <QDebug>
#include <QEventLoop>
#include <QTime>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/un.h>


#define SOCK_REMOTEFILE "/home/root/homeuds"
#define SOCK_LOCALFILE "/home/root/mirroringuds"


#define BUFFER_SIZE 1024

class DAIpcSocket : public QThread
{
    Q_OBJECT

public:
    explicit DAIpcSocket(int mode = 0);
    ~DAIpcSocket();


    void initIpcSocket();
    void sendMessage(QString);

signals:
    void receiveMessage(QString arg);


private:
    int m_mode;

    int m_socket;
    QSocketNotifier *m_pNotifierSocketRead;

    size_t m_sizeAddress;
    char m_bufferReceive[BUFFER_SIZE];

    char m_sockLocalFile[BUFFER_SIZE];
    char m_sockRemoteFile[BUFFER_SIZE];

    struct sockaddr_un m_LocalAddress;
    struct sockaddr_un m_RemoteAddress;


private slots:
    void onReceive(int);
    void run();
};

#endif // DAIPCSOCKET_H
