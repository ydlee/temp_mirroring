import QtQuick 2.5
import QtGraphicalEffects 1.0
import FileIO 1.0

import "./GeneralComponents"
import "./POPUP" as POPUP

Item {

    property bool isConnecting: false
    property string strModelNo: "RSE Model No."
    property alias mirroringConfirm: mirroringConfirm

    property var popupCallback: {
        "":""
    }

    function popupClose()
    {
        main_screen.popup.close();
        backend.isState = 4
    }

    Connections{
        target: main_screen.popup

        onButtonClick:{
            console.log(owner,index,data)

            if(index === 0)
            {
                popupCallback[owner].callbackOne();
            }
            else if(index === 1)
            {
                popupCallback[owner].callbackTwo();
            }
            else if(index === 2)
            {
                popupCallback[owner].callbackThree();
            }
            else if(index === -1)
            {
                console.log("timeout!");
                popupCallback[owner].timeout();
            }
            else if(index === -2)
            {
                popupCallback[owner].bgClick();
            }
            else if(index === -3)
            {
                popupCallback[owner].closeRequest();
            }
            else
            {
                console.log("popupcallback error!!!",owner,index,data)
            }

        }
        onTimeout:{
            main_screen.popup.close();
            backend.isState = 4
        }
    }

    function popupRequestNetworkWillDisconnect(mode)
    {
        console.log(JSON.stringify(servicesetup.status))

        /*
        if(servicesetup.status.wifi.level === 0)
        {
            isConnecting = true
            backend.isSelectMode = mode
            return;
        }
        */

        popupCallback["popupRequestNetworkWillDisconnect"] = {
            "callbackOne" : function() {
                isConnecting = true
                //backend.isState = mode
                popupClose();
            }
            /*
            "callbackTwo" : function() {
                popupClose();
            }
            */
        }

        var parms = {
            useTitle : false,
            useIcon : true,
            iconurl : "qrc:/theme/popup_icon_info.png",
            useBottom : false,
            useContent : true,
            useTimer: true,
            bodyContent : qsTrId("ts_mirroing_disconnect") + ts.empty,// "The hotspot connection has been disconnected by the phone.",
            //buttons : ["Go home"],
            //useBottom : true,
        }
        main_screen.popup.open("popupRequestNetworkWillDisconnect",parms)
    }

    FileIO {
        id: fileIO
        source: "modelNo"
        onError: console.log(msg)
    }

    Component.onCompleted: {
        var randomNum = Math.ceil(Math.random() * 100)
        strModelNo = "RSE-" + randomNum
        fileIO.write(strModelNo)
        fileIO.changeModelNoForMiracast(strModelNo)
        console.log( strModelNo)
    }

    //Mirroring Icon
    Image {
        id: icon
        source: theme.image.mmirror_icon_mirroring
        x: 471
        y: 133

        //for debugging
        /*
        BeepMouseArea
        {
            anchors.fill: parent
            onClicked: {
                backend.isControl = false
                backend.isState = 2
            }
        }
        */


    }

    //Model No
    Text {
        id: titleText
        anchors.left: parent.left
        anchors.right: parent.right
        y: 326
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: strModelNo
        color: "#4ef1ff"
        font.pixelSize: 35
    }

    //Android Phone select button
    Image{
        source: backend.isSelectMode === 0 ? theme.image.mmirror_btn_pre : theme.image.mmirror_btn_nor
        x: 171
        y: 458
        visible: (backend.isState == 0)
        Image {
            source: backend.isSelectMode === 0 ? theme.image.popup_radio_btn_sel:  theme.image.popup_radio_btn_nor
            height: 44
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: 61
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
            horizontalAlignment: Image.AlignLeft


            Text {
                text: qsTrId("ts_android_phone") + ts.empty
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.leftMargin: 30
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "#dadada"
                font.pixelSize: 35
            }

        }

        BeepMouseArea{
            anchors.fill: parent
            onClicked: {
                //popupRequestNetworkWillDisconnect(0)
                backend.isSelectMode = 0
                mirroringConfirm.visible = true;
            }
        }
    }

    //iPhone select button
    Image{
        source: backend.isSelectMode === 1 ? theme.image.mmirror_btn_pre : theme.image.mmirror_btn_nor
        x: 655
        y: 458
        visible: (backend.isState == 0)
        Image {
            source: backend.isSelectMode === 1 ? theme.image.popup_radio_btn_sel : theme.image.popup_radio_btn_nor
            height: 44
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: 61
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
            horizontalAlignment: Image.AlignLeft


            Text {
                text: qsTrId("ts_iphone") + ts.empty
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.leftMargin: 30
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "#dadada"
                font.pixelSize: 35
            }

        }
        BeepMouseArea{
            anchors.fill: parent
            onClicked: {
                //popupRequestNetworkWillDisconnect(1)
                backend.isSelectMode = 1
                mirroringConfirm.visible = true;
            }
        }

    }

    //Loading animation
    Image {
        x: (parent.width / 2) - (width/2)
        y: 458
        property int pre_cnt: 1
        property int cnt: (pre_cnt%11) + 1
        visible: (backend.isState == 1) || (backend.isState == 3)
        source: theme.image["list_loading_circle_%1".arg(String(cnt).length === 1?'0' + String(cnt):cnt)]
        NumberAnimation on pre_cnt {
            id : loading
            loops: Animation.Infinite
            from: 0; to : 20
            running: true
            duration: 1500
            property var old_now: value
        }
    }

    //
    Text {
        id: infoTexts
        anchors.left: parent.left
        anchors.right: parent.right
        y:586
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: (backend.isState == 3) ? backend.phoneName.toString()+qsTrId("ts_displayed")+ts.empty : qsTrId("ts_phone_ready_connected")+ts.empty

        color: "#000000"
        font.pixelSize: 35
        layer.enabled: true
        layer.effect: ColorOverlay {
            color: "#dadada"
        }


        //style: Text.Raised;
        //styleColor: "#dadada"
        //renderType: Text.NativeRendering
//        antialiasing: false
//        font.kerning: false
//        font.preferShaping: false


    }

    Connections {
        target: backend
        onOpenPopup:
        {
            popupRequestNetworkWillDisconnect(0)
        }
    }

    POPUP.MirroringConfirm {
        id: mirroringConfirm
    }



    /*
    LevelAdjust {
        anchors.fill: infoTexts
        source: infoTexts
        minimumInput: "#dadad9"
        maximumInput: "#dadadb"
        maximumOutput: "#dadadb"
    }
    Blend {
        anchors.fill: infoTexts
        source: infoTexts
        foregroundSource: butterfly
        mode: "subtract"
    }
    */



}
