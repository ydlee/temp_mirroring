#include "DAUtilities.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>

bool commandFilter(QString jsonString, QStringList list)
{
    QJsonDocument doc = QJsonDocument::fromJson(jsonString.toUtf8());

    if(!doc.isObject())
    {
        qDebug()  << __FUNCTION__ << "json parsing fail";
        return false;
    }

    QJsonObject obj = doc.object();
    QString command = obj.value("COMMAND").toString();
    if(command.isEmpty()){
        qDebug()  << __FUNCTION__ << "json object dosn't have command key";
        return false;
    }

    if(list.indexOf(command) != (-1))
    {
        return true;
    }
    return false;
}

QString commandUpdateDurationFilter(QString jsonString)
{
    QJsonDocument doc = QJsonDocument::fromJson(jsonString.toUtf8());

    if(!doc.isObject())
    {
        qDebug()  << __FUNCTION__ << "json parsing fail";
        return "";
    }

    QJsonObject obj = doc.object();
    QString command = obj.value("COMMAND").toString();
    if(command.isEmpty()){
        qDebug()  << __FUNCTION__ << "json object dosn't have command key";
        return "";
    }

    if(command.compare("UPDATE_DURATION") != 0){
        return "";
    }

    QString duration = obj.value("TARGET").toString();
    if(duration.isEmpty()){
        qDebug()  << __FUNCTION__ << "json object dosn't have command key";
        return "";
    }

    return duration;
}


QString replaceJson(QString jsonString, bool tf)
{
    QJsonDocument doc = QJsonDocument::fromJson(jsonString.toUtf8());

    if(!doc.isObject())
    {
        qDebug()  << __FUNCTION__ << "json parsing fail";
        return "";
    }

    QJsonObject obj = doc.object();
    QString command = obj.value("TARGET").toString();
    if(command.isEmpty()){
        return "";
    }else {

        if(tf)
        {
            command = command.replace("/mnt/rse/","/media/");
        }else {
            command = command.replace("/media/","/mnt/rse/");
        }


        obj.insert("TARGET",command);
        QJsonDocument doc;
        doc.setObject(obj);
        return doc.toJson(QJsonDocument::Compact);
    }

}
QString getJsonValue(QString jsonString, QString key)
{
    QJsonDocument doc = QJsonDocument::fromJson(jsonString.toUtf8());

    if(!doc.isObject())
    {
        qDebug()  << __FUNCTION__ << "json parsing fail";
        return "";
    }

    QJsonObject obj = doc.object();
    QString ret = obj.value(key).toString();

    return ret;
}
QString setJsonValue(QString jsonString, QString val)
{
    return setJsonValue(jsonString,"COMMAND",val);
}
QString setJsonValue(QString jsonString, QString key, QString val)
{
    QJsonDocument doc;
    doc = QJsonDocument::fromJson(jsonString.toUtf8());

    QJsonObject obj;
    if(doc.isObject())
    {
        obj = doc.object();
    }
    doc.setObject(obj);
    obj.insert(key,QJsonValue::fromVariant(val));
    doc.setObject(obj);
    return doc.toJson(QJsonDocument::Compact);

}

