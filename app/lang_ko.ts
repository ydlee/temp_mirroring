<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>Control</name>
    <message>
        <location filename="Control.qml" line="167"/>
        <source>Android Phone</source>
        <translation type="finished">안드로이드폰</translation>
    </message>
    <message>
        <location filename="Control.qml" line="206"/>
        <source>iPhone</source>
        <translation type="finished">아이폰</translation>
    </message>
    <message>
        <location filename="Control.qml" line="253"/>
        <source> is about to the displayed</source>
        <translation type="finished">이(가) 곧 표시됩니다.</translation>
    </message>
    <message>
        <location filename="Control.qml" line="253"/>
        <source>The selected phone is ready to be wirelessly connected.</source>
        <translation type="finished">선택한 폰을 무선으로 연결할 준비가 되었습니다.</translation>
    </message>
</context>
<context>
    <name>Mirroring</name>
    <message>
        <location filename="Mirroring.qml" line="40"/>
        <source>Mirroring</source>
        <translation type="finished">미러링</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>port for binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>secret for binding</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
