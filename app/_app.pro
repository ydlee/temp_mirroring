#TARGET = mirroring
#QT = quick websockets multimedia
##LIBS += -lhomescreen -lqtappfw

#RESOURCES += \
#    mirroring.qrc \
#    GeneralComponents/sub.qrc

#RESOURCES += \
#       main_sample_qml.qrc

#DEFINES += LINUX_ARM7_ENABLE=1 \
#           LINUX_X86_ENABLE=0 \

#DEFINES += TARGET_STR=\\\"$$TARGET\\\"
#PKGCONFIG += libhomescreen qlibwindowmanager
#include(app.pri)

#LIBS += -lhomescreen -lqtappfw

##LIBS += -lhomescreen \
##        -lqtappfw \


#HEADERS += \
#        AppMirroring.h \
#        CFormCtrl.h \
#        CFormView.h \
#        CUtils.h \
#        CCommon.h \
#        CBrokerEvent.h \
#        CLogger.h \
#        CIPCService.h \
#        CThirdPartyLib.h  \
#        CBeep.h \
#        CBottombar.h \
#        CScriptLanguage.h \
#        CPopup.h \
#        CScriptPython.h \
#        sharddata.h \
#        CLibMirroring.h \
#        fileio.h \
#        #    backend.h \
#    #    DAIpcSocket.h \
#    #    DAUtilities.h \
#    #    GeneralComponents/setupservice.h \
#    #    GeneralComponents/theme.h \
#    #   GeneralComponents/resource.h
    
#SOURCES += \
#        main.cpp \
#        AppMirroring.cpp \
#        CFormCtrl.cpp \
#        CFormView.cpp \
#        CUtils.cpp \
#        CCommon.cpp \
#        CBrokerEvent.cpp \
#        CLogger.cpp \
#        CIPCService.cpp \
#        CThirdPartyLib.cpp \
#        CBeep.cpp \
#        CBottombar.cpp \
#        CScriptPython.cpp \
#        CScriptLanguage.cpp \
#        CPopup.cpp \
#        CLibMirroring.cpp \
#         fileio.cpp \
#        #    backend.cpp \
#        #    DAUtilities.cpp \
#        #    DAIpcSocket.cpp \
#        #    GeneralComponents/setupservice.cpp \
#        #    GeneralComponents/theme.cpp

##=====================================================================
TARGET = mirroring
QT = quick websockets multimedia
LIBS += -lhomescreen -lqtappfw

include(app.pri)


SOURCES +=  \
        main.cpp \
        AppMirroring.cpp \
        CFormCtrl.cpp \
        CFormView.cpp \
        CUtils.cpp \
        CCommon.cpp \
        CBrokerEvent.cpp \
        CLogger.cpp \
        CIPCService.cpp \
        CThirdPartyLib.cpp \
        CBeep.cpp \
        CBottombar.cpp \
        CScriptPython.cpp \
        CScriptLanguage.cpp \
        CPopup.cpp \
        CLibMirroring.cpp \
         fileio.cpp \
        #    backend.cpp \
        #    DAUtilities.cpp \
        #    DAIpcSocket.cpp \
        #    GeneralComponents/setupservice.cpp \
        #    GeneralComponents/theme.cpp



HEADERS += \
        AppMirroring.h \
        CFormCtrl.h \
        CFormView.h \
        CUtils.h \
        CCommon.h \
        CBrokerEvent.h \
        CLogger.h \
        CIPCService.h \
        CThirdPartyLib.h  \
        CBeep.h \
        CBottombar.h \
        CScriptLanguage.h \
        CPopup.h \
        CScriptPython.h \
        sharddata.h \
        CLibMirroring.h \
        fileio.h \
        #    backend.h \
    #    DAIpcSocket.h \
    #    DAUtilities.h \
    #    GeneralComponents/setupservice.h \
    #    GeneralComponents/theme.h \
    #   GeneralComponents/resource.h


CONFIG += link_pkgconfig
PKGCONFIG += libhomescreen qlibwindowmanager


DEFINES += TARGET_STR=\\\"$$TARGET\\\"
DEFINES += LINUX_ARM7_ENABLE=1 \
           LINUX_X86_ENABLE=0 \

RESOURCES += \
    mirroring.qrc \
    GeneralComponents/sub.qrc

RESOURCES += \
       main_sample_qml.qrc
##=====================================================================

