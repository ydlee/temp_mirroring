///////////////////////////////////////////////////////////
//  CPopup.h
//  Implementation of the Class CPopup
//  Created on:      28-7-2020 오후 2:47:47
//  Original author: user
///////////////////////////////////////////////////////////
#ifndef __CPOPUP_H__
#define __CPOPUP_H__



class CPopup
{

public:
	CPopup();
   virtual ~CPopup();
   virtual void showPopup()=0;
};
#endif // !defined(EA_A96D7CB1_AA6A_4fd0_BC2F_2583CD817DF4__INCLUDED_)
