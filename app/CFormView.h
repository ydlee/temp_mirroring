///////////////////////////////////////////////////////////
//  CFormView.h
//  Implementation of the Class CFormView
//  Created on:      28-7-2020 오후 5:19:43
//  Original author: user
///////////////////////////////////////////////////////////
#ifndef __CFORMVIEW_H__
#define __CFORMVIEW_H__

#include <QQuickView>
#include <QtQml/QQmlApplicationEngine>
#include "sharddata.h"


#if DEBUG_TEST_ENABLE
#include <libhomescreen.hpp>
#include <qlibwindowmanager.h>
#include "pagecontrolrouter.h"
#endif



class CFormView :  public QQmlApplicationEngine
{

public:
    CFormView();
    virtual ~CFormView();
private:
    void init();

 protected:
public:
    int loadQmlUrl(QString strURL);

};

#endif // !defined(EA_C79C1E82_F1B8_4919_9391_6C2C6B70E8C9__INCLUDED_)
