///////////////////////////////////////////////////////////
//  CCommon.h
//  Implementation of the Class CCommon
//  Created on:      28-7-2020 오후 8:29:14
//  Original author: user
///////////////////////////////////////////////////////////
#ifndef __CCOMMON_H__
#define __CCOMMON_H__

#include "CLogger.h"
#include "CUtils.h"
#include "CThirdPartyLib.h"
#include "CBottombar.h"
#include "CPopup.h"
#include "CBeep.h"


class CCommon : public CUtils , public CPopup , public CBottombar , public CBeep
{

public:
    CCommon();
    virtual ~CCommon();

private:
    CThirdPartyLib *m_pThirdPartyLib;



private:
    void init();
    CThirdPartyLib* getThirdPartyCore();


public:
    void showPopup() override;
    void showBottobar() override;
    void playBeep() override;

};

#endif // !defined(EA_1FD0D181_EE44_4790_8AFC_5E019D929042__INCLUDED_)
