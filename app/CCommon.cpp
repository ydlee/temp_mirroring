///////////////////////////////////////////////////////////
//  CCommon.cpp
//  Implementation of the Class CCommon
//  Created on:      28-7-2020 오후 2:46:06
//  Original author: user
///////////////////////////////////////////////////////////

#include "CCommon.h"
#include "CFormCtrl.h"


CLogger* clog = CLogger::Instance();

CCommon::CCommon()
    : CUtils()
    , CPopup()
    , CBottombar()
    , CBeep()

{
    init();
}


 CCommon::~CCommon(){

}

void CCommon::init()
{
      getThirdPartyCore();
}


CThirdPartyLib* CCommon::getThirdPartyCore()
{
    if(nullptr == m_pThirdPartyLib)
        m_pThirdPartyLib = new CThirdPartyLib();
    return m_pThirdPartyLib;

}


void CCommon::showPopup() {

    clog->Log("%s", "CCommon::show_opup()");
}

void CCommon::showBottobar() {
    clog->Log("%s", "CCommon::showBottobar()");
}

void CCommon::playBeep(){
    clog->Log("%s", "CCommon::playBeep()");
//    CFormCtrl*  ctl = (CFormCtrl*)this

}
