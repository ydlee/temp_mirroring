///////////////////////////////////////////////////////////
//  CLogger.h
//  Implementation of the Class CLogger
//  Created on:      28-7-2020 오후 2:48:41
//  Original author: user
///////////////////////////////////////////////////////////

#ifndef __CLOGGER_H__
#define __CLOGGER_H__

//#include <dlt.h>
#include <QDebug>

#include <stdio.h>
#include <string.h>
#include <QObject>
#include <iostream>
#include "sharddata.h"




class CLogger
{


public:
   static CLogger* Instance();

private:
    CLogger();
    virtual ~CLogger();
    static CLogger* m_pLogThis;
private:
    char* log(const char* format, ...);
public:
    void logInfo(QString qstrMsg);
    void logDebug(QString qstrMsg);
    void Log(const char * format, ...);

#if DEBUG_TEST_TEMP_ENABLE
    void logging(Args ... args);
#endif
};
#endif // !defined(EA_024DEA65_C152_4d72_98FE_A0D7E7197309__INCLUDED_)
