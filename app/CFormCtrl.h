///////////////////////////////////////////////////////////
//  CFormCtrl.h
//  Implementation of the Class CFormView
//  Created on:      28-7-2020 오후 5:19:43
//  Original author: user
///////////////////////////////////////////////////////////

#ifndef __CFORMCTRL_H__
#define __CFORMCTRL_H__


#include <QObject>


#if LINUX_ARM7_ENABLE
//#include "pagecontrolrouter.h"
#include <libhomescreen.hpp>
#include <qlibwindowmanager.h>
#include "GeneralComponents/setupservice.h"
#include "fileio.h"
#endif
#include "sharddata.h"
#include "CFormView.h"
#include "CCommon.h"
#include "CBrokerEvent.h"




class CFormCtrl
        : public CBrokerEvent
        , public CCommon
{

    Q_OBJECT
#if DEBUG_TEST_QML_TEMP_ENABLE
    // Temp >> Test
    Q_PROPERTY(int num READ getnum WRITE setnum NOTIFY numchanged)

//    Q_PROPERTY(int isSelectMode READ getIsSelect WRITE setIsSelectMode NOTIFY isSelectModeChanged)
//    Q_PROPERTY(int isState READ getIsState WRITE setIsState NOTIFY isStateChanged)
//    Q_PROPERTY(int isBG READ getIsBG WRITE setIsBG NOTIFY isBGChanged)
//    Q_PROPERTY(QString phoneName READ getPhoneName WRITE setPhoneName NOTIFY isPhoneNameChanged)
    Q_PROPERTY(bool show_popup READ show_popup WRITE setShowPopup NOTIFY showPopupChanged)

#endif

public:
    CFormCtrl();
    virtual ~CFormCtrl();


private:
    CFormView*  m_pCFormView;
    void init();
    QProcess* m_process;

private:
   bool m_showPopup;
#if LINUX_ARM7_ENABLE
       QLibWindowmanager*  m_windowManager;
       LibHomeScreen*      m_homeScreen;
#endif
public:
    CFormView* getViewPtr();
    setupservice* m_setup ;

    int setModeInVisible();
    void setIsBG(int value);
    int setModeVisible();
    void call_run_Engine();

    bool show_popup();
    void setShowPopup(bool);
    void slot_run_Engine();

    void socketInit(int port, const char *secret);
    int  windowManagerInit(int port, const char *secret);
    int  homeScreenInit(int port, const char *secret);
    int  QmlInit(QString strQml="main.qml");

#if DEBUG_TEST_QML_TEMP_ENABLE
    void setnum(const int &);
    int  getnum() const;
    int m_NumValue;


signals:
    void numchanged();
    void signal_run_Engine();
    void showPopupChanged();

#endif



public:
#if LINUX_X86_ENABLE
    void printState(int n) ;
    void func2(QString res);
#endif
    void setTitle(QString qstrTitle)
    {
       CBrokerEvent::m_myname = qstrTitle;
    }


};

#endif // !defined(EA_C79C1E82_F1B8_4919_9391_6C2C6B70E8C9__INCLUDED_)

