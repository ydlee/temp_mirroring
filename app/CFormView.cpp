///////////////////////////////////////////////////////////
//  CFormView.cpp
//  Implementation of the Class CFormView
//  Created on:      28-7-2020 오후 5:19:43
//  Original author: user
///////////////////////////////////////////////////////////

#include "CFormView.h"
#include "CLogger.h"
#include <QQuickWindow>
#include <QObject>

CFormView::CFormView()
    : QQmlApplicationEngine()
{
 //TODO
        init();
}

CFormView::~CFormView(){
//TODO
}

 void CFormView::init(){
     //TODO
 }

int CFormView::loadQmlUrl(QString strURL){

     QString qstrUrl = nullptr;

    if(strURL.indexOf("qrc:/") < 0)
         qstrUrl = QString("qrc:/%1").arg(strURL);
    else
         qstrUrl = strURL;

    this->load(QUrl(qstrUrl));

#if 0 //DEBUG_TEST_TEMP_ENABLE
    if(1){
        //Desktop - "Hello World" :
        this->load(QUrl(QStringLiteral("qrc:/helloword_sample.qml")));
    }

//    CLogger::Instance()->logInfo(qstrUrl);

#endif

    return  (int)rootObjects().size();
}

