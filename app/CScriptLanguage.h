///////////////////////////////////////////////////////////
//  CScriptLanguage.h
//  Implementation of the Class CScriptLanguage
//  Created on:      30-7-2020 오후 11:52:27
//  Original author: user
///////////////////////////////////////////////////////////

#ifndef __CSCRIPTLANGUAGE_H__
#define __CSCRIPTLANGUAGE_H__

#include "CScriptPython.h"

class CScriptLanguage : public CScriptPython
{

public:
	CScriptLanguage();
	virtual ~CScriptLanguage();

};
#endif // !defined(EA_79AA8E79_8048_4e8a_A11F_A9AEBCF324F3__INCLUDED_)
