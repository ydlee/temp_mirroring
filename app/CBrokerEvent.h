///////////////////////////////////////////////////////////
//  CBrokerEvent.h
//  Implementation of the Class CBrokerEvent
//  Created on:      28-7-2020 오후 11:09:46
//  Original author: user
///////////////////////////////////////////////////////////
#ifndef __CBROKEREVENT_H__
#define __CBROKEREVENT_H__


#include <qthread.h>
#include <QtCore/QUrlQuery>
#include "sharddata.h"
#include "CIPCService.h"
#include <QMutex>

#if LINUX_ARM7_ENABLE
#include <qtappfw/messageengine.h>
#include <qtappfw/modemanager.h>
#include <qtappfw/bottombar.h>
#include <qtappfw/homescreen.h>
#include <qtappfw/soundmanager.h>
#include "DAIpcSocket.h"
#endif

class CBrokerEvent : public QObject , public CIPCService
{

public:
    CBrokerEvent(QObject *parent=nullptr);
	virtual ~CBrokerEvent();

private:
    void init();
    QMutex  m_dataMutex;
protected:
    QString m_myname;
#if LINUX_ARM7_ENABLE
    MessageEngine *m_pMessageEngine;
    SoundManager*  m_pSoundmanager;
    ModeManager*   m_pModemanager;
    HomeScreen*    m_pHomescreen ;
    BottomBar*     m_pBottombar ;
#endif
  DAIpcSocket* m_pIpcSocket ;// = new ;

protected:
    int  webSocktInit(QStringList positionalArguments);
    int  webSocktInit(int port, const char *chSecret);
    int  messageEngineInit(QUrl& url);


    void set_websocket(QString url);

#if LINUX_X86_ENABLE
   void onConnected();
   void onTextMessageReceived(QString message);
#endif

 private:
    void MessageSend();
    void onReceive(int value);
};

#endif // !defined(EA_A8BC7FD2_6705_4ab7_808C_BD6614A3549F__INCLUDED_)
