#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include "DAIpcSocket.h"
#include <QProcess>
#include "GeneralComponents/setupservice.h"
#include <libhomescreen.hpp>
#include <qlibwindowmanager.h>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <qtappfw/modemanager.h>
#include <qtappfw/messageengine.h>
#include <qtappfw/soundmanager.h>


#define NOTCONNECT  0
#define CONNETTING  1
#define CONNECTED   2
#define SHOW_PHONENAME_BEFORECONNECTED   3
#define GOHOME   4

#define NOTBACKGROUND     0
#define BACKGROUND        1

#define MODE_NONE           0
#define MODE_VISIBLE        1
#define MODE_INVISIBLE      2

#define ANDROUD         0
#define IPHONE          1

class BackEnd : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int isSelectMode READ getIsSelect WRITE setIsSelectMode NOTIFY isSelectModeChanged)
    Q_PROPERTY(int isState READ getIsState WRITE setIsState NOTIFY isStateChanged)
    Q_PROPERTY(int isBG READ getIsBG WRITE setIsBG NOTIFY isBGChanged)
    Q_PROPERTY(QString phoneName READ getPhoneName WRITE setPhoneName NOTIFY isPhoneNameChanged)
    Q_PROPERTY(bool showPopup READ showPopup WRITE setShowPopup NOTIFY showPopupChanged)

public:
    explicit BackEnd(QGuiApplication *app);
    ~BackEnd();

    void reInit(QString name);

    int getIsSelect() const;
    int getIsState() const;
    bool getIsBG() const;
    QString getPhoneName() const;

    void setIsSelectMode(int value);
    void setIsState(int value);
    void setIsBG(int value);
    void setPhoneName(QString value);

    Theme* get_theme();
    setupservice* get_setupservice();

    void set_websocket(QString url);

    int displaySetting();

    double CheckWIFIrssi();
    int CheckWIFIonAirplay();

    //for homescreen refactoring
    int setModeVisible();
    int setModeInVisible();
    int setModeActive();
    int setModeInActive();

    int setHS(LibHomeScreen* hs);
    int setWM(QLibWindowmanager* qwm);
    int setMM(ModeManager* mm);    
    int setSM(SoundManager* sm);

    int get_Phonename();

    void goHomelauncher();

    bool showPopup();
    void closePopup();
    void setShowPopup(bool);

    Q_INVOKABLE int     call_run_Engine();
    Q_INVOKABLE void    onScreenReply(QString);    

public slots:
    Q_INVOKABLE void play();

    void ExtCmdProcedure(QString cmd);
    void ExtProcess_State();
    void slotBackButtonClicked();

    //for process
    void finished(int exitCode, QProcess::ExitStatus exitStatus);
    void readyReadStandardError();
    void readyReadStandardOutput();

    void onReceive(int);

    void slot_run_Engine();

signals:
    void isSelectModeChanged();
    void isStateChanged();
    void isBGChanged();
    void popupBackButtonClicked();
    void isPhoneNameChanged();

    void signal_run_Engine();

    void openPopup();
    void showPopupChanged();


private:
    DAIpcSocket *m_pIpcSocket;
    int m_isSelectMode;//android / iphone
    bool m_isControl;
    bool m_isConnected;
    int m_isState;//Not connect / conneting / connected
    int m_isBG;//background
    QProcess *m_process;
    setupservice *m_setup;
    QLibWindowmanager *m_qwm;
    LibHomeScreen *m_hs;
    ModeManager *m_mm;
    SoundManager *m_sm;

    int m_isModeState;//visible / not visible
    QString m_Phonename;
    bool m_showPopup;

    int run_Engine();


};

#endif // BACKEND_H
