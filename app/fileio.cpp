#include "fileio.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>

FileIO::FileIO(QObject *parent) :
    QObject(parent)
{

}

QString FileIO::read()
{
    if (mSource.isEmpty()){
        emit error("source is empty");
        return QString();
    }

    QFile file(mSource);
    QString fileContent;
    if ( file.open(QIODevice::ReadOnly) ) {
        QString line;
        QTextStream t( &file );
        do {
            line = t.readLine();
            fileContent += line;
         } while (!line.isNull());

        file.close();
    } else {
        emit error("Unable to open the file");
        return QString();
    }
    return fileContent;
}

bool FileIO::write(const QString& data)
{
    if (mSource.isEmpty())
        return false;

    QFile file(mSource);
    if (!file.open(QFile::WriteOnly | QFile::Truncate))
        return false;

    QTextStream out(&file);
    out << data;

    file.close();

    return true;
}

//Workaround
//시리얼번호를 입력하기 전까지 랜덤번호를 사용하는 함
bool FileIO::changeModelNoForMiracast(QString randomNo)
{
    QByteArray fileData;
    QFile file("/home/root/kivic/wpa.conf");
    file.open(QIODevice::ReadWrite); // open for read and write
    fileData = file.readAll(); // read all the data into the byte array
    QString text(fileData); // add to text string for easy string replace

    QStringList textList= text.split("\n");
    int listCount = text.split("\n").length();
    qDebug() << "listCount : " << listCount;

    QString findStr, replaceStr;

    for(int i=0; i<listCount; i++)
    {
        if(textList[i].contains("device_name="))
        {
            findStr = textList[i];
            break;
        }
    }
    qDebug() << "findStr : " << findStr;

    replaceStr = "device_name=" + randomNo;
    qDebug() << "replaceStr : " << replaceStr;


    text.replace(findStr, replaceStr); // replace text in string


    file.seek(0); // go to the beginning of the file
    file.write(text.toUtf8()); // write the new text back to the file

    file.close(); // close the file handle.
    return true;
}
