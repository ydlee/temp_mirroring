import QtQuick 2.0

import '../COMPONENT' as COMPONENTS

Item {
    property real brightnessPopupW:parent.width
    property real brightnessPopupH:parent.height

    id: popup
    visible: false
    width: 1280
    height: 800

    Connections {
        target: backend
        onPopupBackButtonClicked:
        {
            popup.visible = false;
        }
    }

    Image {
        id:backgroundImage
        width: 980
//        height: 331
        height: 344

        source: theme.image.popup_btn_line4_bg
        x:150
//        y:192
        y:188
    }

    Image {
        x : 595
        y : 147
        width  : 90
        height : 90
        source : theme.image.popup_icon_warning
    }

    Text {
        x : backgroundImage.x + 42
        y : backgroundImage.y + 62
        width  : 900
        height : 300

        lineHeight: 0.75
        
        wrapMode:Text.Wrap

        horizontalAlignment: Text.AlignHCenter

        color: "#ffffff"

        font.pixelSize: 35

        text : qsTrId("ts_no_wifi_no_mirroring1") + "\n"
        + qsTrId("ts_no_wifi_no_mirroring2") + "\n"
        + qsTrId("ts_req_wifi_off") + "\n"
        + qsTrId("ts_press_wifi_2") + ts.empty


    }

    COMPONENTS.ImgBtn {
        id:wifiBtn
        width: 464
        height: 71
        x:168
        y:440

        imgsrc: theme.image.popup_2btn_display_nor
        pushedImgSrc: theme.image.popup_2btn_display_pre

        btnText: qsTrId("ts_wifi") + ts.empty
        btnTextSize: 35

        onButtonClick:  {
            backend.onScreenReply("setup:wifi")
            parent.visible = false;
        }
    }

    COMPONENTS.ImgBtn {
        id:noBtn
        width: 464
        height: 71
        x:648
        y:440

        imgsrc: theme.image.popup_2btn_display_nor
        pushedImgSrc: theme.image.popup_2btn_display_pre

        btnText: qsTrId("ts_no") + ts.empty
        btnTextSize: 35

        onButtonClick:  {
            backend.call_run_Engine();
            parent.visible = false;
        }
    } 

    onVisibleChanged: {
        if (visible) {
            backend.showPopup = true;
        } else {
            backend.showPopup = false;
        }
    }       
}
