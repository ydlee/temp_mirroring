///////////////////////////////////////////////////////////
//  CScriptPython.h
//  Implementation of the Class CScriptPython
//  Created on:      28-7-2020 오후 2:47:03
//  Original author: user
///////////////////////////////////////////////////////////
#ifndef __CSCRIPTPYTHON_H__
#define __CSCRIPTPYTHON_H__



//#include <Python.h>
#include <QString>

class CScriptPython
{

public:
	CScriptPython();
	virtual ~CScriptPython();

    void PyRun(QString fileName , QString func );
    void init();
    int pyRunfile(char* filename);

};
#endif // !defined(EA_5A684E61_3342_4ff9_A091_628890807AC6__INCLUDED_)
