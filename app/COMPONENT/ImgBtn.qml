import QtQuick 2.6

Item {
    id: imgbtn_item
    //color: "transparent"

    property string btnText: ""
    property real btnTextSize: 22
    property string btnTextColor:"#ffffff"

    property url imgsrc: ""
    property url selectedImgSrc: ""
    property url pushedImgSrc: ""
    property url disableImgSrc: ""



    property real btnTrigerIntervalTime: 500

    activeFocusOnTab: true
    enabled: true

    signal buttonClick()
    height: parent.height
    Component.onCompleted:
    {



    }

    Rectangle{
       anchors.fill: imgbtn_item
       color: "transparent"
       Image {
           id: img
           width: parent.width
           height: parent.height
           source: imgsrc
           cache: false
           //fillMode: Image.PreserveAspectFit
           verticalAlignment: Image.AlignVCenter
           horizontalAlignment: Image.AlignHCenter

       }
       Text {
           id: txt
           width: parent.width
           height: parent.height
           text: btnText
           font.pixelSize: btnTextSize
           color: btnTextColor
           verticalAlignment: Image.AlignVCenter
           horizontalAlignment: Image.AlignHCenter

       }

       MouseArea {
           id: mouse
           width: parent.width
           height: parent.height

           onClicked: {
               if(imgbtn_item.enabled)
                    buttonClick()
               checkClickedBtnTimer.stop()
               beep.play()
           }

           onPressed: {
               console.log("Timer start")

               checkClickedBtnTimer.start()
           }
       }
   }

    Timer {
        id: checkClickedBtnTimer
        interval: btnTrigerIntervalTime
        running: false
        repeat: true
        onTriggered: {
            if((!imgbtn_item.focus) && (imgbtn_item.enabled) && (mouse.pressed)) {
                 console.log("emit clicked event")
                 buttonClick();
            }

            else {
                console.log("stop clicked event")
                checkClickedBtnTimer.stop()
            }
        }
    }

   states: [
       State { //Normal
           when: (!imgbtn_item.focus) && (imgbtn_item.enabled) && (!mouse.pressed)
           PropertyChanges { target: img; source: imgsrc }
       },
       State {//Pushed
           when: (!imgbtn_item.focus) && (imgbtn_item.enabled) && (mouse.pressed)
           PropertyChanges { target: img; source: pushedImgSrc }
       },
       State {//Focus
           when: (imgbtn_item.focus) && (imgbtn_item.enabled) && (!mouse.pressed)
           PropertyChanges { target: img; source: selectedImgSrc }
       },
       State {//Disable
           when: (!imgbtn_item.enabled) && (!mouse.pressed);
           PropertyChanges { target: img; source: disableImgSrc }
       }
   ]

}
