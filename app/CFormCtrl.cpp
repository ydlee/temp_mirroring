///////////////////////////////////////////////////////////
//  CFormView.cpp
//  Implementation of the Class CFormView
//  Created on:      28-7-2020 오후 5:19:43
//  Original author: user
///////////////////////////////////////////////////////////
#include "CFormCtrl.h"
#include "CLogger.h"


CFormCtrl::CFormCtrl()
    : CBrokerEvent()
    , CCommon()
    , m_pCFormView(nullptr)
{
    //** Todo **
    init();
}

CFormCtrl::~CFormCtrl(){
    //** Todo **
    delete m_pCFormView;
#if LINUX_ARM7_ENABLE
    delete m_windowManager;
    delete m_homeScreen;
#endif
}

void CFormCtrl::init()
{
#if DEBUG_TEST_QML_TEMP_ENABLE
     m_NumValue= 0;
#endif

#if LINUX_ARM7_ENABLE
     m_windowManager = new QLibWindowmanager();
     m_homeScreen    = new LibHomeScreen();
#endif
     m_process = new QProcess();

//    connect(this, SIGNAL(isStateChanged()), this, SLOT(ExtProcess_State()));
//    connect(this, SIGNAL(isBGChanged()), this, SLOT(ExtProcess_State()));
//    connect(this, SIGNAL(isPhoneNameChanged()), this, SLOT(ExtProcess_State()));
//    connect(this, SIGNAL(signal_run_Engine()), this, SLOT(slot_run_Engine()));

}


CFormView* CFormCtrl::getViewPtr(){
    if( nullptr == m_pCFormView)
         m_pCFormView = new CFormView();
    return m_pCFormView;
}


#if DEBUG_TEST_QML_TEMP_ENABLE
int CFormCtrl::getnum() const
{
    qDebug() << "[1] called getnum()";
    return m_NumValue;
}

void CFormCtrl::setnum(const int &gn ){
    qDebug() << "[2] called getnum()";
    m_NumValue = gn;
    emit numchanged();
}
#endif

void CFormCtrl::socketInit(int port, const char *secret)
{
    CBrokerEvent::webSocktInit(port, secret);
    windowManagerInit(port, secret);
    homeScreenInit(port, secret);
    CBrokerEvent::messageEngineInit(CIPCService::m_url);
}


int CFormCtrl::QmlInit(QString strQml)
{
    QQmlContext *context = NULL;
    context =  getViewPtr()->rootContext();

#if LINUX_ARM7_ENABLE

//    FileListModel listModel;
//    FileListModel gridModel;
//    FileListModel allModel;

//    context->setContextProperty("windowmanager", m_windowManager);
//    context->setContextProperty("backend", m_backend);
//    context->setContextProperty("bottombar", m_bottombar);
//    context->setContextProperty("homescreen", m_homescreen);
//    context->setContextProperty("udsManager", m_udsManager);
//    context->setContextProperty("mediaManager", m_mediaManager);
//    context->setContextProperty("eventManager", m_eventManager);
//    context->setContextProperty("fileManager", m_fileManager);
//    context->setContextProperty("listModel", &listModel);
//    context->setContextProperty("gridModel", &gridModel);
//    context->setContextProperty("allModel", &allModel);
//    context->setContextProperty("beep", m_pSoundmanager);
//    context->setContextProperty("servicesetup", setup);
//    context->setContextProperty("ts", setup);


    // for Bottombar
    context->setContextProperty("homescreen", m_pHomescreen);
    context->setContextProperty("bottombar", m_pBottombar);
    context->setContextProperty("windowmanager", m_windowManager);

    // Declaring your C++ class to the QML system
    qmlRegisterType<FileIO, 1>("FileIO", 1, 0, "FileIO");

    if(1) {
        getViewPtr()->loadQmlUrl(strQml);//
    }
    else {
      getViewPtr()->loadQmlUrl("helloword_sample.qml");
    }

#endif

#if LINUX_X86_ENABLE
        // >> Desktop - "Hello World" :
      context->setContextProperty("LDS", this);
      getViewPtr()->loadQmlUrl("helloword_sample.qml");

#endif

    return  (int)getViewPtr()->rootObjects().size();
}




#if LINUX_X86_ENABLE

void CFormCtrl::printState(int n)
{
    qDebug() << "CFormCtrl::printState::"<< n;
//    CLogger::getInstance()->logInfo(&QString::number(n));

    func2("aaa");
}

void CFormCtrl::func2(QString res)
{
    QString res1 = "CFormCtrl::func2 succeeded";
    qDebug() << res <<":"<< res1;
    CLogger::Instance()->Log("%s","func2");

    return;
}
#endif

void CFormCtrl::setIsBG(int value)
{
#if 0
    //if(m_isBG == value)
    //    return;

    qDebug() << "[SY]" << __FUNCTION__<<"value : " << value;
    m_isBG = value;

    emit isBGChanged();
#endif
}

//Visible
int CFormCtrl::setModeVisible()
{

#if 0

    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    m_setup->updateSettings();

    setIsBG(NOTBACKGROUND);

    m_isModeState = MODE_VISIBLE;
    if( m_isState == CONNECTED )
    {
        setIsBG(NOTBACKGROUND);
        //alphablend
        system("modetest -M nexell -w 18:alphablend:12");
        qDebug() << "[SY] m_isState == CONNECTED";

        if(m_isSelectMode == IPHONE)
        {
            CheckWIFIonAirplay();
        }
    }
#endif
}

//InVisible
int CFormCtrl::setModeInVisible()
{
#if 0
    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;
//    CLogger::Log();


    m_isModeState = MODE_INVISIBLE;

    setIsBG(BACKGROUND);

    if( m_isState == CONNECTED )
    {
        setIsBG(BACKGROUND);
        //alphablend off
        system("modetest -M nexell -w 18:alphablend:16");
        qDebug() << "[SY] m_isState == CONNECTED";
    }
    else if ( m_isState == CONNETTING )
    {
        system("killall kivicmain &");
        QThread::msleep(100);
        system("killall gst-launch-1.0 &");
        QThread::msleep(100);
        if(m_isSelectMode == ANDROUD)
        {
            system("killall wpa_cli");
            QThread::msleep(300);
            system("killall wpa_supplicant");
            QThread::msleep(300);
            system("killall udhcpd");
            QThread::msleep(300);
            system("systemctl restart connman &");
            QThread::msleep(300);
        }
        setIsState(NOTCONNECT);
    }
#endif
}

void CFormCtrl::call_run_Engine() {
    emit signal_run_Engine();
}

bool CFormCtrl::show_popup() {
    return m_showPopup;
}

void CFormCtrl::slot_run_Engine()
{
    run_Engine(m_process);
}

void CFormCtrl::setShowPopup(bool bShowPopup) {
    m_showPopup = bShowPopup;
    emit showPopupChanged();
}


int CFormCtrl::windowManagerInit(int port, const char *secret){


    CFormCtrl* backend = this;
    QString myname = this->m_myname ;

#if LINUX_ARM7_ENABLE

    if(m_windowManager->init(port, secret) != 0){
        exit(EXIT_FAILURE);
    }

    //>> Request a surface as described in layers.json windowmanager’s file
    if (m_windowManager->requestSurface(myname) != 0) {
        exit(EXIT_FAILURE);
    }

     //>> Create an event callback against an event type. Here a lambda is called when SyncDraw event occurs
    m_windowManager->set_event_handler(QLibWindowmanager::Event_SyncDraw, [this, myname](json_object *object) {
        m_windowManager->endDraw(myname);
    });

    m_windowManager->set_event_handler(QLibWindowmanager::Event_Visible, [backend,myname](json_object *object){
        json_object *obj;
        json_object_object_get_ex(object, "drawing_name",&obj);
        QString label = QString(json_object_get_string(obj));

        if(label == myname)
        {
            qDebug() << "[Sherlock]" << __FUNCTION__ << "Event_Visible";
            //Mode change
             backend->setModeVisible();
        }
    });

    m_windowManager->set_event_handler(QLibWindowmanager::Event_Invisible, [backend,myname](json_object *object){
        json_object *obj;
        json_object_object_get_ex(object, "drawing_name",&obj);
        QString label = QString(json_object_get_string(obj));

        if(label == myname)
        {
            qDebug() << "[Sherlock]" << __FUNCTION__ << "Event_Invisible";
            //Mode change
            backend->setModeInVisible();
        }
    });

#endif
    return 0;
}



int CFormCtrl::homeScreenInit(int port, const char *secret)
{
#if LINUX_ARM7_ENABLE
    CFormCtrl* backend = this;// ((CFormCtrl*)this);
    m_homeScreen->init(port, secret);
    QString myname = this->m_myname;

    //>> Set the event handler for Event_TapShortcut which will activate the surface for windowmanager
    m_homeScreen->set_event_handler(LibHomeScreen::Event_TapShortcut, [this, myname](json_object *object){
        json_object *appnameJ = nullptr;
        if(json_object_object_get_ex(object, "application_name", &appnameJ))
        {
            const char *appname = json_object_get_string(appnameJ);
            if(myname == appname)
            {
                m_windowManager->activateSurface(myname);

            }
        }
    });

    m_homeScreen->set_event_handler(LibHomeScreen::Event_OnScreenReply, [this,backend](json_object *object){
        json_object *response = nullptr;
        if(json_object_object_get_ex(object, "reply_message", &response)) {
            const char *message = json_object_get_string(response);

            if(strcmp(message, "mirroring:no") == 0)
            {
                // 처리
                qDebug() << "LibHomeScreen::Event_OnScreenReply : " << message;
                backend->call_run_Engine();

            }

        }
    });

#endif

    return 0;
}



