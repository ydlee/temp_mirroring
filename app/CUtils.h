///////////////////////////////////////////////////////////
//  CUtils.h
//  Implementation of the Class CUtils
//  Created on:      28-7-2020 오후 2:46:24
//  Original author: user
///////////////////////////////////////////////////////////

#ifndef __CUTILS_H__
#define __CUTILS_H__

#include "sharddata.h"
#include "CScriptPython.h"
#include "DAUtilities.h"
#include <QProcess>


class CUtils
{

public:
	CUtils();
    virtual ~CUtils();

private:
    CScriptPython* m_pScriptPython;

public :
    void init();
    CScriptPython* getScriptPython();

    QString CurrentDateTime();
    QString currentPath();
    int run_Engine(QProcess*  process);
    int get_Phonename();


};

#endif // !defined(EA_31A6B126_5033_4292_AB6C_DDD3077DCFF4__INCLUDED_)
