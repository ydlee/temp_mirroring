///////////////////////////////////////////////////////////
//  CBeep.h
//  Implementation of the Class CBeep
//  Created on:      28-7-2020 ���� 2:47:20
//  Original author: user
///////////////////////////////////////////////////////////


#ifndef __CBEEP_H__
#define __CBEEP_H__

#include "sharddata.h"

#if DEBUG_TEST_ENABLE
#include <qtappfw/soundmanager.h>
#endif

class CBeep
{

public:
	CBeep();
    virtual ~CBeep();

public:
     virtual void playBeep()=0;

};
#endif // !defined(EA_6FEFAC37_2C27_4db8_8553_3CF7E59D4DA9__INCLUDED_)
