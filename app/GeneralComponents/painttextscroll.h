﻿#ifndef PAINTTEXTSCROLL_H
#define PAINTTEXTSCROLL_H

#define SPEED    2
#define INTERVAL 40
#define TURN_INTERVAL 2000
#define SPACE 100

#include <QObject>
#include <QQuickPaintedItem>
#include <QPainter>
#include <QFont>
#include <QTimer>
#include <QTextItem>
#include <QDebug>

class PaintTextScroll : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)
    Q_PROPERTY(bool defalutCenter READ defalutCenter WRITE setDefalutCenter NOTIFY defalutCenterChanged)

public:

    PaintTextScroll();
    void paint(QPainter *painter);

    QString text() const;
    void setText(QString text);

    QString color() const;
    void setColor(QString color);

    int fontSize() const;
    void setFontSize(int size);

    bool defalutCenter() const;
    void setDefalutCenter(bool value);

signals:
    void textChanged();
    void colorChanged();
    void fontSizeChanged();
    void defalutCenterChanged();
    void requestUpdate();
    void checkTimer();

public slots:
    void timeout();
    void setTimer();

private:
    QString m_text;
    QString m_color;
    int m_fontSize;
    bool m_defalutCenter;
    int m_xPosForText;
    bool m_moving;
    bool m_turn;
    QTimer m_timer;
};

#endif // PAINTTEXTSCROLL_H
