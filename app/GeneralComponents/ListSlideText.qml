﻿import QtQuick 2.5

Item {
    id: root
    property alias slideText: t1.text
    property alias textColor: t1.color
    property alias elide: t1.elide
    property bool slide: false
    property bool moving: false
    property int fontPixelSize
    property int spacing: 50
    property int textSize: Math.ceil(t1.paintedWidth)
    property int movingArea: Math.ceil(t1.paintedWidth) - width

    clip: true

    Text {
        id: t1
        width: parent.width
        height: parent.height
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        font.pixelSize: fontPixelSize
        text: slideText

        SequentialAnimation on x {
            id: animation; running: false; loops: Animation.Infinite
            NumberAnimation { from: 0; to: -movingArea; duration: 7000 }
            PauseAnimation { duration: 300 }
            NumberAnimation { from: -movingArea; to: 0; duration: 7000 }
            PauseAnimation { duration: 300 }
        }
    }

    onMovingChanged: {
        animation.running = false

        if (moving) {
            t1.elide = Text.ElideNone
            slide = (movingArea > 0) ? true : false

            if (slide) {
                animation.running = true
            } else {
                t1.elide = Text.ElideRight
            }

        } else {
            t1.elide = Text.ElideRight
            t1.x = 0
        }
    }
}
