﻿import QtQuick 2.5

Item {
    id: popup; visible: false
    anchors.fill: parent

    property alias stringList: repeater.model
    property alias popupBg: popupBg.source
    property bool showCheckBox: false
    property bool disableText: true

    property string check_box: "qrc:/GeneralComponents/Resource/check_box.png"
    property string uncheck_box: "qrc:/GeneralComponents/Resource/uncheck_box.png"
    property string drop_popup_bg1: "qrc:/GeneralComponents/Resource/drop_popup_bg1.png"
    property string drop_popup_bg2: "qrc:/GeneralComponents/Resource/drop_popup_bg2.png"
    property string drop_popup_bg3: "qrc:/GeneralComponents/Resource/drop_popup_bg3.png"
    property string drop_popup_bg4: "qrc:/GeneralComponents/Resource/drop_popup_bg4.png"
    property string drop_popup_pre: "qrc:/GeneralComponents/Resource/drop_popup_pre.png"

    signal firstPopupClicked()
    signal secondPopupClicked()
    signal thirdPopupClicked()
    signal fourthPopupClicked()
    signal disTextClicked()

    onVisibleChanged: {
        if (visible) {
            visibleTimer.restart()
        } else {
            visibleTimer.stop()
        }
    }
    onDisTextClicked: {
        visibleTimer.restart()
    }

    Rectangle{
        anchors.fill: parent
        color: "#000000"
        opacity: 0.7
    }

    MouseArea {
        anchors.fill: parent

        onClicked: {
            popup.visible = false
        }
    }

    Timer {
        id: visibleTimer
        repeat: false
        interval: 10000
        onTriggered: {
            popup.visible = false
        }
    }

    Image {
        id: popupBg

        x: 868; y: 41   //x: 838; y: 41

        Column {
            x: 23; y: 49  //x: 53; y: 49
            Repeater {
                id : repeater
                Item {
                    id: popupItem
                    width: 373; height: 74
                    Image {
                        id: checkBox; visible: false
                        x: 17; y: 14; width: 47; height: 47
                        source: (mouseArea.pressed) ? check_box : uncheck_box
                    }
                    Rectangle {
                        id: tmpBg
                        visible: false
                        anchors.fill: parent
                        color: "transparent"
                        MouseArea {
                            enabled: tmpBg.visible
                            anchors.fill: parent
                            onClicked: disTextClicked()
                        }
                    }
                    Image {
                        id: select
                        width: 373; height: 74; visible: false
                        source: drop_popup_pre
                    }
                    Text {
                        id: text
                        x: 20; y: 18; width: 333; height: 39
                        verticalAlignment: Text.AlignVCenter
                        text: modelData
                        color: "#dadada"
                        font.pixelSize: 35
                    }
                    BeepMouseArea {
                        id: mouseArea
                        anchors.fill: parent

                        onContainsMouseChanged: {
                            if (!containsMouse) {
                                text.color = "#dadada"
                                select.visible = false
                            }
                        }
                        onPressed: {
                            if (containsMouse) {
                                select.visible = true
                                text.color = "#09fffc"
                            }
                        }
                        onReleased: {
                            text.color = "#dadada"
                            select.visible = false

                            if (containsMouse) {
                                switch (index) {
                                case 0:
                                    firstPopupClicked()
                                    break
                                case 1:
                                    secondPopupClicked()
                                    break
                                case 2:
                                    thirdPopupClicked()
                                    break
                                case 3:
                                    fourthPopupClicked()
                                    break
                                }
                                popup.visible = false
                            }
                        }
                    }

                    states : [
                        State {
                            name: "checkBox"; when: (showCheckBox)
                            PropertyChanges { target: checkBox; visible: true }
                            PropertyChanges { target: text; x: 83; width: 271; height: 39 }
                        },
                        State {
                            name: "disText"; when: ((text.text == (qsTrId("ts_subtitle") + ts.empty) || text.text == (qsTrId("ts_show_current_track")) + ts.empty) && disableText)
                            PropertyChanges { target: select; visible: false }
                            PropertyChanges { target: tmpBg; visible: true }
                            PropertyChanges { target: text; color: "#686868" }
                            PropertyChanges { target: mouseArea; enabled: false }
                        }
                    ]
                }

                onModelChanged: {
                    switch(count) {
                    case 1:
                        popupBg.source = drop_popup_bg1
                        break
                    case 2:
                        popupBg.source = drop_popup_bg2
                        break
                    case 3:
                        popupBg.source = drop_popup_bg3
                        break
                    case 4:
                        popupBg.source = drop_popup_bg4
                        break
                    }
                }
            }
        }
    }
}

