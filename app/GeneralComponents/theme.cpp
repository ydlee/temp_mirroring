#include "theme.h"
#include <QJsonObject>
#include "resource.h"

Theme::Theme()
{
    _image = &def_theme;
}

Theme::~Theme()
{

}

QJsonObject Theme::image(){
    return *_image;
}

void Theme::setTheme(QString str){
    if(QString::compare(str,QString("def")) == 0){
        _image = &def_theme;
    }else{
        _image = &kids_theme;
    }
    emit imageChanged();
}

QString Theme::tr(){
    return _tr;
}


void Theme::setTr(){
    _tr = QString("");
    emit trChanged();
    _tr = QString("%1");
    emit trChanged();
}
