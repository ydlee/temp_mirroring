import QtQuick 2.5
import QtGraphicalEffects 1.0

import '../GeneralComponents'


Item {
    id : root
    visible: false
    anchors.fill: parent
    property var source
    property string owner: ""
    property var params: {"":""}

    signal buttonClick(string owner,int index,var data)
    signal backgroundClick(string owner)
    signal timeout()

    signal beepClicked()

    function open(name,args){
        owner = name
        params = Object(args);
        root.visible = true
        if(Boolean(params.useTimer))
        {
            if(Boolean(params.timerInterval))
            {
                view.timerSetInterval(params.timerInterval)
            }
            else
            {
                view.timerSetInterval(3000)
            }
            view.timerStart()
        }
        else
        {
            view.timerStop()
        }
    }

    function close(){
        params = {"":""}
        visible = false
    }

    function closeRequest(){
        root.buttonClick(root.owner,-3,"")
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            root.buttonClick(root.owner,-2,"")
            backgroundClick(root.owner)
        }
    }

    Rectangle {
        anchors.fill:parent
        color: "#000000"
        opacity: 0.5
    }

    Item{
        width: parent.width
        height: parent.height
        y : 0
        Behavior on y { SmoothedAnimation { velocity: 600 } }

        Popupview{
            id: view
            useTitle: Boolean(params)?params.useTitle:false
            useIcon: Boolean(params)?params.useIcon:true
            useBottom: Boolean(params)?params.useBottom:false
            useContent:Boolean(params)?params.useContent:true
            iconurl:Boolean(params)?params.iconurl:theme.image.popup_icon_warning
            bodyContent:Boolean(params)?params.bodyContent:"Warning!"
            titletext:Boolean(params)?params.titletext:"This is title"
            buttons:Boolean(params)?params.buttons:["Cancel","Ok"]
            inputTexts:Boolean(params)?params.inputTexts:null
            popupWidth: Boolean(params.popupWidth)?params.popupWidth:980
            useLoader:  Boolean(params.useLoader)?params.useLoader:false
            loaderSource:  Boolean(params)?params.loaderSource:""
            onButtonClick: {
                view.timerStop()
                root.buttonClick(root.owner,index,inputTextsData)
            }
            onTimeout: {
                console.log("popup","timeout!!")
                root.close();
                root.buttonClick(root.owner,-1,inputTextsData)
                root.timeout()
            }
            onBeepClicked: {
                root.beepClicked()
            }
        }
    }


}
