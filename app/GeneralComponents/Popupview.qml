import QtQuick 2.5
import QtQuick.Layouts 1.1
import '../GeneralComponents'

Item {


    z: 101
    id : root

    signal buttonClick(int index)
    signal timeout()
    signal beepClicked()

    property int popupWidth: (980)
    property bool useTitle: true
    property bool useIcon: false
    property bool useBottom: true
    property bool useContent: true
    property bool useLoader: false
    property bool useTheme: Boolean(theme)
    property string iconurl : theme.image.popup_icon_warning
    property string bodyContent : "Warning!"
    property string titletext: "This is title"
    property var buttons: ["Cancel","Ok"]
    property var inputTexts: ["label1","label2"]
    property var inputTextsData: ["","","",""]
    property url loaderSource: "qrc:/GeneralComponentsonent/GridItem.qml"

//    onUseTimerChanged: {
//        if(useTimer)
//        {
//            timer.stop()
//            console.log("timer start")
//            timer.start()
//        }
//        else
//        {
//            timer.stop()
//        }
//    }

    function timerStart()
    {
        console.log("popup","timerStart!!")
        timer.restart()
    }

    function timerStop()
    {
        console.log("popup","timerStop!!")
        timer.stop()
    }

    function timerSetInterval(interval)
    {
        timer.interval = interval
    }

    Timer {
        id: timer

        repeat: false
        onTriggered: {
            timeout()
        }
    }



    BorderImage {
        id: name
        source: useTheme ?
                    theme.image.popup_txt_line1_bg:
                    "qrc:/theme/popup_txt_line1_bg.png"
        anchors.fill: parent
        border.left: 20; border.top: 20
        border.right: 20; border.bottom: 20
    }
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    width: root.popupWidth
    height: view.height
    MouseArea{
        anchors.fill: parent
        onClicked: {}
    }

    Column {
        id : view
        width: parent.width
        Item{
            visible: useIcon
            height: 0
            width: parent.width
            Image{
                visible: iconurl !== (useTheme ?
                             theme.image.popup_icon_loading_01:
                             "qrc:/theme/popup_icon_loading_01.png")

                //popup_icon_loading_01
                source: iconurl
                y : -height/2
                width: (90)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                MouseArea{
                    anchors.fill:parent
                    onClicked: {}
                }
            }

            Image {
                visible: iconurl === (useTheme ?
                                          theme.image.popup_icon_loading_01:
                                          "qrc:/theme/popup_icon_loading_01.png")
                y : -height/2
                width: (90)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                property int pre_cnt: 1
                property int cnt: (pre_cnt%11) + 1
                source: useTheme ?
                            theme.image["popup_icon_loading_%1".arg(String(cnt).length === 1?'0' + String(cnt):cnt)] :
                            "qrc:/theme/popup_icon_loading_%1.png".arg(String(cnt).length === 1?'0' + String(cnt):cnt)
                NumberAnimation on pre_cnt {
                    id : loading
                    loops: Animation.Infinite
                    from: 0; to : 11
                    running: true
                    duration: 1000
                }
            }
        }

        Item{
            visible: useTitle
            id : header
            height: (80)
            width: parent.width

            Text{
                color:"#fff"
                text: root.titletext
                font.pixelSize: (35)
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
            }

            Image {
                anchors.bottom : parent.bottom
                width: parent.width
                height: 1
                source: useTheme?
                            theme.image.list_line :
                            "qrc:/theme/list_line.png"
            }

        }



        Item{
            width: parent.width
            height: content.height + (content.parent.anchors.topMargin + content.parent.anchors.bottomMargin)
            id : body

            Item{
                anchors.fill: parent
                anchors.topMargin: (85) - (useTitle?23:0) - (useBottom?31:0)
                anchors.bottomMargin: (85) - (useBottom?76:0)
                anchors.leftMargin: (40)
                anchors.rightMargin: (40)

                Column{
                    id :content
                    width: parent.width
                    Text{
                        visible: useContent
                        id : contentText
                        text : root.bodyContent
                        color:"#fff"
                        font.pixelSize: (35)
                        wrapMode:Text.Wrap
                        width: parent.width

                        // 폰트 변경으로 내용물이 많이 긴 팝업 콘텐츠는 bottombar 영역을 침범하는 경우가 있음 현대 기아 폰트에 마추어 해당 경우의 라인높이 변경
                        lineHeight: (text.length > 100)?0.75:1

                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                    }

                    Loader{
                        anchors.horizontalCenter: parent.horizontalCenter
                        id : popupLoader
                        visible: useLoader
                        source: useLoader?root.loaderSource:""
                    }

                    Repeater{
                        model : root.inputTexts

                        Item{
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: (560)
                            height: (64) + (index != root.inputTexts.length - 1?(15):0)
                            Text{
                                color:"#fff"
                                text : modelData
                                font.pixelSize: (26)
                            }

                            Image {
                                y : (33)
                                source: theme.image.popup_textbox
                                width: parent.width
                                height: (32)

                                TextInput{
                                    id : textinput
                                    anchors.fill: parent
                                    anchors.topMargin: (3)
                                    anchors.bottomMargin: (3)
                                    anchors.leftMargin: (11.5)
                                    anchors.rightMargin: (11)
                                    color : "#999999"
                                    text : ""
                                    font.pixelSize: (22)
                                    layer.enabled: true
                                    verticalAlignment: TextInput.AlignVCenter

                                    onTextChanged: {
                                        inputTextsData[index] = text
                                    }

                                }
                            }
                        }

                    }

                }
            }

        }


        Item{
            id:bottom
            width: parent.width
            visible: useBottom
            height: 90
            Row{
                id: buttonRow
                y : 9
                spacing: 16
                anchors.horizontalCenter: parent.horizontalCenter
                Repeater{
                    model: root.buttons
                    Image{
                        source: useTheme?
                                    (button.containsMouse?theme.image["popup_%1btn_pre".arg(root.buttons.length !== 3?1:3)]:theme.image["popup_%1btn_nor".arg(root.buttons.length !== 3?1:3)]) :
                                    (button.containsMouse?"qrc:/theme/popup_%1btn_pre.png".arg(root.buttons.length !== 3?1:3):"qrc:/theme/popup_%1btn_nor.png".arg(root.buttons.length !== 3?1:3))
                        height: (71);
                        width: (sourceSize.width)
                        BeepMouseArea{
                            id:button
                            anchors.fill: parent
                            onClicked: {
                                beepClicked()
                                buttonClick(index)
                            }
                        }

                        Text{
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            color:"#fff"
                            font.pixelSize: (35)
                            text : modelData
                        }

                    }
                }


            }
        }

    }

}
