#ifndef SETUPSERVICE_H
#define SETUPSERVICE_H

#include <QObject>
#include <QWebSocket>
#include <QUrl>
#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <QTranslator>
#include <QtGui/QGuiApplication>
#include "qqmlapplicationengine.h"
#include "theme.h"

class setupservice: public QWebSocket
{
    Q_OBJECT

    enum MsgEnum
    {
        CALL = 2,
        RETOK,
        RETERR,
        EVENT,
    };

    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QJsonObject conf READ conf WRITE setConf NOTIFY confChanged)
    Q_PROPERTY(QJsonObject status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(bool autoget READ autoget WRITE setAutoget NOTIFY autogetChanged)
    Q_PROPERTY(QJsonArray targets READ targets WRITE setTargets NOTIFY targetsChanged)
    Q_PROPERTY(QString empty READ empty NOTIFY updateLanguage)

public:
    setupservice(QGuiApplication *app,bool useStatus = false);
    ~setupservice();

    void updateSettings();

    QString url();
    void setUrl(QString);

    QJsonObject conf();
    void setConf(QJsonObject);

    QJsonObject status();
    void setStatus(QJsonObject);

    QJsonArray targets();
    void setTargets(QJsonArray);

    bool autoget();
    void setAutoget(bool);

    Q_INVOKABLE void get_common();
    Q_INVOKABLE void get_status(QString);
    Q_INVOKABLE void set_common(QJsonObject);
    Q_INVOKABLE void set_common_property(QString,QString);
    Q_INVOKABLE void reset_common();
    Q_INVOKABLE void init_target(QString);
    Q_INVOKABLE void get_target(QString);
    Q_INVOKABLE void set_target(QString,QJsonObject);
    Q_INVOKABLE void set_target_property(QString,QString,QString);

//    void set_engin(QQmlApplicationEngine *engine);
//    void set_themeTarget(Theme *theme);
    void set_app(QGuiApplication *);
    void pre_read_target(QString);
    void update_theme(QString);
    void update_language(QString);
    QString empty();
    Theme* get_theme();

private:
    const QString apiString = "agl-service-setup";
    const QString payloadLength = "9999";
    QList<QString> WaitingCommand;

    QString _url;
    QString theme_name;
    QJsonObject _conf;
    QJsonObject _status;
    bool _autoget = false;
    QVariantList _targets;

    int retry = 0;
    const int max_retry = 20;

    bool m_useStatus = false;

    void command(QString,QJsonObject &);

//    QQmlApplicationEngine *_engine;
    QGuiApplication *_app;
    Theme *_theme = new Theme();
    QTranslator *translator = new QTranslator();


private slots:
    void serviceHandler(QString message);
    void serviceStateChanged(QAbstractSocket::SocketState state);
    void serviceConnected();
    void serviceDisconnected();
    void serviceUrlChanged(QString);


signals:
    void dead();
    void urlChanged(QString url);
    void confChanged(QJsonObject conf);
    void statusChanged(QJsonObject status);
    void autogetChanged(bool autoget);
    void targetsChanged(QJsonArray targets);
    void getConfString(QString val);
    void themeChange();
    void confItemChanged(QString target_name);
    void updateLanguage();

};

#endif // SETUPSERVICE_H
