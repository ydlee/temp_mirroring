import QtQuick 2.5

Item {
    width: 700
    height: parent.height
    property alias icon: icon.source
    property alias title: text.text
    property bool isKids: servicesetup.conf.common.theme === "kids"


    Image{
        id: icon
        visible: isKids
        width: 70
        height: 78
        x : 24
        y : 11
    }

    Text{
        id: text
        x : isKids ? 110 : 40
        y : isKids ? 23 : 21
        font.pixelSize: isKids ? 44 : 35
        color : "#ffffff"
        style: isKids ? Text.Outline : Text.Normal; styleColor: "#1a4db7";
        font.bold: isKids
    }

}
