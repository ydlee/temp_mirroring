/*
 * Copyright (C) 2016 The Qt Company Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Window 2.2

Rectangle {
    property real btnSize     : Screen.height * 0.08625
    property real stateBtnSize: Screen.height * 0.04625

    property string wifiInfo: servicesetup.status.wifi.info
    property int wifiLevel  : servicesetup.status.wifi.level
    property int wifiRssi   : servicesetup.status.wifi.rssi

    property string bluetoothInfo : servicesetup.status.bluetooth.isConnected
    property string recentBluetoothInfo : ""

    property var currentAudioModeIcon: {
//        "aux" : [theme.image.bottom_bar_icon_audio_out_earphone_nor,theme.image.bottom_bar_icon_audio_out_earphone_pre],
//        "bluetooth" : [theme.image.bottom_bar_icon_audio_out_bt_nor,theme.image.bottom_bar_icon_audio_out_bt_pre]
        "aux" : ["qrc:/GeneralComponents/Resource/bottom_bar_icon_audio_out_earphone_nor.png", "qrc:/GeneralComponents/Resource/bottom_bar_icon_audio_out_earphone_pre.png"],
        "bluetooth" : ["qrc:/GeneralComponents/Resource/bottom_bar_icon_audio_out_bt_nor.png", "qrc:/GeneralComponents/Resource/bottom_bar_icon_audio_out_bt_pre.png"]
    }

    width: Screen.width
    height: Screen.height * 0.1075
    color: "#01010100"

    onBluetoothInfoChanged: {
        if(bluetoothInfo === "true")    // show bluetooth icon
        {
            bluetoothStateBtn.visible = true
            if(bluetoothInfo !== recentBluetoothInfo) {         // audio path to bluetooth
                fileAudioSwitcher.setAudioPath("bluetooth")
                recentBluetoothInfo = String(bluetoothInfo)
            }
        }
        else                            // hide bluetooth icon
        {
            bluetoothStateBtn.visible = false
            if(bluetoothInfo !== recentBluetoothInfo) {         // audio path to audio
                fileAudioSwitcher.setAudioPath("audio")
                recentBluetoothInfo = String(bluetoothInfo)
            }
        }
    }

    onWifiInfoChanged: {
        var isConnect = wifiInfo.split(" ")[0]

        // if Wifi is connected array[0] set to connected
        // else array[0] set to not
        if(isConnect === "connected" && wifiRssi !== -1) {
            wifiStateBtn.visible = true
        }
        else {            
            wifiStateBtn.visible = false
        }
    }

    onWifiLevelChanged:  {
        console.log("WifiLevel Changed : "+ wifiLevel)
    }

    onWifiRssiChanged: {
        if(wifiRssi !== -1)
        {
            var Strength = wifiRssi + 120;
            Strength = Strength > 100 ? 100 : Strength;

            if(Strength >= 80)      // wifi icon max
            {
                wifiStateBtn.source = "qrc:/GeneralComponents/Resource/bottom_indi_icon_wifi.png" // theme.image.bottom_indi_icon_wifi
            }
            else if (Strength >= 60)
            {
                wifiStateBtn.source = "qrc:/GeneralComponents/Resource/bottom_indi_icon_wifi_03.png" // theme.image.bottom_indi_icon_wifi_03
            }
            else if (Strength >= 40)
            {
                wifiStateBtn.source = "qrc:/GeneralComponents/Resource/bottom_indi_icon_wifi_02.png" // theme.image.bottom_indi_icon_wifi_02
            }
            else
            {
                wifiStateBtn.source = "qrc:/GeneralComponents/Resource/bottom_indi_icon_wifi_01.png" // theme.image.bottom_indi_icon_wifi_01
            }
        }
        else
        {
            wifiStateBtn.source = "qrc:/GeneralComponents/Resource/bottom_indi_icon_no_wifi.png" // theme.image.bottom_indi_icon_no_wifi
        }
    }

    AudioSwitcher {
        id:fileAudioSwitcher
    }


    Rectangle {
        anchors.fill: parent
        color: "#010101"
        id:btbItems

        Image {
            id: bottombarTopLine
            source: "qrc:/GeneralComponents/Resource/bg_bottom_line.png"    // source: theme.image.bg_bottom_line
            height: 2
            width: parent.width
            y:0
        }


        Row {
            height: btnSize
            anchors.horizontalCenter: parent.horizontalCenter
            y:parent.height/2 - btnSize/2
            spacing: 20

            ImgBtn {
                id:goHomeBtn
                width: btnSize
                height: btnSize
                imgsrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_home_nor.png"  // theme.image.bottom_bar_icon_home_nor
                pushedImgSrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_home_pre.png" // theme.image.bottom_bar_icon_home_pre
                onButtonClick: {
                    windowmanager.activateSurface("HomeScreen")
                }

                // only Home launcher "homeAndBackKeyVisible" Value is false
                visible: backend.homeAndBackKeyVisible()
            }

            ImgBtn {
                id: displayOffBtn
                width: btnSize
                height: btnSize
                imgsrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_display_off_nor.png" // theme.image.bottom_bar_icon_display_off_nor
                pushedImgSrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_display_off_pre.png" // theme.image.bottom_bar_icon_display_off_pre
                onButtonClick: {
                    bottombar.displayOffButtonClick()
                }
            }

            ImgBtn {
                id: popUpBrightnessBtn
                width: btnSize
                height: btnSize
                imgsrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_brightness_nor.png" // theme.image.bottom_bar_icon_brightness_nor
                pushedImgSrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_brightness_pre.png" // theme.image.bottom_bar_icon_brightness_pre
                onButtonClick: {
                    homescreen.showPopup("General_Brightness_01", "")
                }
            }

            ImgBtn {
                id: popUpVolumeBtn
                width: btnSize
                height: btnSize
                imgsrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_volume_nor.png" // theme.image.bottom_bar_icon_volume_nor
                pushedImgSrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_volume_pre.png" // theme.image.bottom_bar_icon_volume_pre
                onButtonClick: {                    
                    homescreen.showPopup("General_pop_vol_01", "")
                }
            }

            ImgBtn {
                id: audioOut
                width: btnSize
                height: btnSize
                imgsrc:currentAudioModeIcon[fileAudioSwitcher.currentAudioMode][0]
                pushedImgSrc:currentAudioModeIcon[fileAudioSwitcher.currentAudioMode][1]
                onButtonClick: {
                    if(bluetoothInfo === "true") {
                        fileAudioSwitcher.setAudioSwitch()
                    }
                    else {
                        fileAudioSwitcher.setAudioPath("audio")
                    }
                }
            }

            ImgBtn {
                id: goBackBtn
                width: btnSize
                height: btnSize
                imgsrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_back_nor.png" // theme.image.bottom_bar_icon_back_nor
                pushedImgSrc: "qrc:/GeneralComponents/Resource/bottom_bar_icon_back_pre.png" // theme.image.bottom_bar_icon_back_pre
                onButtonClick: {
                    backend.slotBackButtonClicked()
                }

                // only Home launcher "homeAndBackKeyVisible" Value is false
                visible: backend.homeAndBackKeyVisible()
            }

        }

        Row {
            height: btnSize
            x:parent.width - width - stateBtnSize/2
            y:parent.height/2 - stateBtnSize/2
            spacing: 10

            Image {
                id:bluetoothStateBtn
                width: stateBtnSize
                height: stateBtnSize
                visible: false
                source: "qrc:/GeneralComponents/Resource/bottom_indi_icon_bt.png" // theme.image.bottom_indi_icon_bt
            }

            Image {
                id:wifiStateBtn
                width: stateBtnSize
                height: stateBtnSize
                visible: false
                // imgsrc:theme.image.bottom_indi_icon_wifi//"qrc:/res/res/bottom_indi_icon_wifi.png"
                // Behavior on opacity { NumberAnimation { duration : 700 }}

                Text {
                    visible: servicesetup.conf.common.wifidebug === "true"
                    anchors.centerIn: parent
                    font.pixelSize: 25
                    color:"#eeee55"
                    text : wifiRssi
                }
            }
        }

    }
}
