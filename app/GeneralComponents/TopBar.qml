﻿import QtQuick 2.5

Image {
    // property alias icon: icon.source
    property alias title: title.text
    property alias showListIcon: listIcon.visible
    property alias listIconEnable: listIcon.enabled
    property alias listIconSource: listIcon.source
    property alias containsMouseOnList: listIcon.containsMouse
    property alias showMenuIcon: menuIcon.visible
    property alias menuIconEnable: menuIcon.enabled
    property alias menuIconSource: menuIcon.source
    property alias containsMouseOnMenu: menuIcon.containsMouse

    property string top_bar_bg: "qrc:/GeneralComponents/Resource/top_bar_bg.png"
    property string top_bar_icon_list_sel: "qrc:/GeneralComponents/Resource/top_bar_icon_list_sel.png"
    property string top_bar_icon_list_pre: "qrc:/GeneralComponents/Resource/top_bar_icon_list_pre.png"
    property string top_bar_icon_list_nor: "qrc:/GeneralComponents/Resource/top_bar_icon_list_nor.png"
    property string top_bar_icon_list_dis: "qrc:/GeneralComponents/Resource/top_bar_icon_list_dis.png"
    property string top_bar_icon_menu_sel: "qrc:/GeneralComponents/Resource/top_bar_icon_menu_sel.png"
    property string top_bar_icon_menu_pre: "qrc:/GeneralComponents/Resource/top_bar_icon_menu_pre.png"
    property string top_bar_icon_menu_nor: "qrc:/GeneralComponents/Resource/top_bar_icon_menu_nor.png"
    property string top_bar_icon_menu_dis: "qrc:/GeneralComponents/Resource/top_bar_icon_menu_dis.png"

    signal listIconPressed()
    signal listIconClicked()
    signal listIconReleased()
    signal menuIconPressed()
    signal menuIconClicked()
    signal menuIconReleased()

    width: 1280
    height: 94
    source: top_bar_bg

    states: State {
        name: "menuInvisible"; when: showListIcon && !showMenuIcon
        PropertyChanges { target: listIcon; x: 1201 }
    }
    
    MouseArea {     // For preventing PathView clicked
        anchors.fill: parent
    }

    Text {
        id: title
        x: 40
        y: 28
        font.pixelSize: 35
        color : "#ffffff"
    }

    ImageButton{
        id: listIcon
        x: 1095
        y: 11
        source: top_bar_icon_list_nor

        onContainsMouseChanged: {
            if (!containsMouse)
                listIcon.source = top_bar_icon_list_nor
        }
        onPressed: {
            if (containsMouse)
                listIcon.source = top_bar_icon_list_pre
            listIconPressed()
        }
        onReleased: {
            listIcon.source = top_bar_icon_list_nor
            if (containsMouse)
                listIconReleased()
        }
    }
    ImageButton{
        id: menuIcon
        x: 1201
        y: 11
        source: top_bar_icon_menu_nor

        onContainsMouseChanged: {
            if (!containsMouse)
                menuIcon.source = top_bar_icon_menu_nor
        }
        onPressed: {
            if (containsMouse)
                menuIcon.source = top_bar_icon_menu_pre
            menuIconPressed()
        }
        onReleased: {
            menuIcon.source = top_bar_icon_menu_nor
            if (containsMouse)
                menuIconReleased()
        }
    }
}
