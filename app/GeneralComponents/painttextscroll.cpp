﻿#include "painttextscroll.h"

PaintTextScroll::PaintTextScroll(): QQuickPaintedItem()
  , m_text("")
  , m_color("")
  , m_fontSize(0)
  , m_defalutCenter(false)
  , m_xPosForText(0)
  , m_moving(false)
  , m_turn(false)
{
    this->setRenderTarget(QQuickPaintedItem::FramebufferObject);
    connect(this,SIGNAL(requestUpdate()),this,SLOT(update()));
    connect(this,SIGNAL(checkTimer()),this,SLOT(setTimer()));
    connect(&m_timer,SIGNAL(timeout()),this,SLOT(timeout()));

    m_timer.setTimerType(Qt::PreciseTimer);
    //moveToThread(this->thread());
    //this->thread()
}

void PaintTextScroll::paint(QPainter *painter)
{
    int width = static_cast<int>(boundingRect().width());
    int height = static_cast<int>(boundingRect().height());

    QRect rect;
    QFont font;

    font = painter->font();
    font.setPixelSize(m_fontSize);
    painter->setFont(font);

    QFontMetrics fm(font);
    rect.setX(m_xPosForText);
    rect.setY(0);
    rect.setWidth(width);
    rect.setHeight(height);

    //painter->setRenderHint(QPainter::SmoothPixmapTransform);


    int fontWidth = fm.width(m_text);
    painter->setPen(QColor(m_color));
    if(fontWidth > width)
    {
        if( m_xPosForText <= (-fontWidth) - SPACE)
        {
            m_xPosForText = 0;
            m_turn = true;
        }
        rect.setX(m_xPosForText);
        rect.setWidth(fontWidth);
        painter->drawText(rect, Qt::AlignVCenter|Qt::AlignLeft ,m_text);


        rect.setX( m_xPosForText + fontWidth + SPACE );
        rect.setWidth(fontWidth);
        painter->drawText(rect, Qt::AlignVCenter|Qt::AlignLeft ,m_text);

        m_moving = true;

    }else
    {
        if(m_defalutCenter)
        {
            painter->drawText(rect, Qt::AlignCenter ,m_text);
        }
        else
        {
            painter->drawText(rect, Qt::AlignVCenter ,m_text);
        }
        m_moving = false;

    }

    emit checkTimer();

}

QString PaintTextScroll::text() const
{
    return m_text;
}

void PaintTextScroll::setText(QString text)
{
    if(m_text == text)
        return;
    m_text = text;
    emit textChanged();
    m_xPosForText = 0;
    m_turn = false;
    emit requestUpdate();
}

QString PaintTextScroll::color() const
{
    return m_color;
}

void PaintTextScroll::setColor(QString color)
{
    if(m_color == color)
        return;
    m_color = color;
    emit colorChanged();
}

int PaintTextScroll::fontSize() const
{
    return m_fontSize;
}

void PaintTextScroll::setFontSize(int size)
{
    if(m_fontSize == size)
        return;
    m_fontSize = size;
    emit fontSizeChanged();
}
bool PaintTextScroll::defalutCenter() const
{
    return m_defalutCenter;
}

void PaintTextScroll::setDefalutCenter(bool value)
{
    if(m_defalutCenter == value)
        return;
    m_defalutCenter = value;
    emit defalutCenterChanged();
}

void PaintTextScroll::setTimer()
{

    if(m_moving && m_timer.isActive() && (m_timer.interval() == (TURN_INTERVAL)))
    {
        m_timer.stop();
        m_timer.start(INTERVAL);
        return;
    }

    if(m_moving && !m_timer.isActive())
    {
        m_timer.start(INTERVAL);
        return;
    }else if(!m_moving && m_timer.isActive())
    {
        m_timer.stop();
        return;
    }

    else if(m_moving && m_turn && (m_timer.interval() == (INTERVAL)))
    {
        m_timer.stop();
        m_timer.start(TURN_INTERVAL);
        m_turn = false;
        return;
    }


}
void PaintTextScroll::timeout()
{
    m_xPosForText-=SPEED;
    emit requestUpdate();
}
