# mx_rse_comp

## 애플리케이션에 적용하기

우선 애플리케이션의 디렉토리에 공용 컴포넌트의 서브디렉토리를 추가합니다.

```shell
$ cd {mx_rse_myapp}
$ git submodule add https://gitlab.com/drimaes/mx_rse_comp app/GeneralComponents
# 안될때는
# git submodule add -f https://gitlab.com/drimaes/mx_rse_comp app/GeneralComponents
```

app.pro 파일에 리소스를 추가해줍니다.

```
RESOURCES += \
    qml.qrc \
    GeneralComponents/sub.qrc
```

## 공용컴포넌트 업데이트하기

서브 모듈의 디렉토리에서 수정해 주면 됩니다.

```shell
$ cd {mx_rse_myapp}/app/GeneralComponents
$ git pull
```
___
## Popup(팝업 사용하기) 작성중..

main.qml 파일에 Popup을 배치 해줍니다.

```qml
// main.qml
import "./GeneralComponents"

// Window 안쪽에
property alias popup: popup
Popup{
  id : popup
  source:frame
}
```
여기서 frame은 블러 효과를 입힐 컴포넌트를 대상으로 사용해야합니다. Window 파일에 나뉘어져 있을때는 Item으로 묶어주는걸 추천드립니다.

추가로 뒤로가기 버튼을 눌렀을때

### 팝업 열기

```qml
onClicked : {
  var parms = {
    useTitle : false,
    useIcon : true,
    iconurl : theme.image.popup_icon_warning
    useContent : true,
    bodyContent:"이거는 내용물입니다.\n줄바꿈도 됩니다.",
    useBottom : true,
    buttons : ["Ok"],// 버튼 3개를 쓰고 싶다면 ["1","2","3"],
    useTimer: true //timeout 설정
    timerInterval : 3000 // defalut: 3000
  }

  window.popup.open("owner name",parms)
}
```


### 버튼 클릭 이벤트 받기

Connection을 이용해서 따로 받아 낼 수 있습니다.

```qml
Connections{
    target: window.popup

    onButtonClick:{
        console.log(owner,index,data)
        if(owner === "owner name" && index == 0){
            console.log("첫번째 버튼이 클릭 되었습니다.")
        }
    }

    onBackgroundClick:{// 배경이 눌렸을떄 팝업을 닫게 할 수 있습니다.
      window.popup.close()
    }
    onTimeout: {
        colsole.log("timeout")
    }
}
```
___
## DropPopup 컴포넌트 사용법
### property 세팅
|              | default |
|--------------|:-------:|
|       z      |    X    |
|    visible   |  false  |
|  stringList  |    X    |
| showCheckBox |  false  |
|  disableText |  false  |

* 컴포넌트 사용할 때 `z` 값 유의  
* Text 세팅한 후에 `visible = true`로 세팅  
* `stringList`에 Text 추가해야 dropPopup 보임  
* checkBox 사용할 때 `showCheckBox = true`로 세팅
* 현재 List가 `Show current track`이면 `disableText = true`로 세팅  

### DropPopup Text 세팅
```qml
dropPopupId.stringList = ["one", "two", "three", "four"]
```
* Background 이미지는 `stringList` 갯수에 맞춰 세팅됨

### 터치 이벤트 커스텀
```qml
onFirstPopupClicked: {}
onSecondPopupClicked: {}
onThirdPopupClicked: {}
onFourthPopupClicked: {}
```
___
## ScrollBar 컴포넌트 사용법
```qml
ListView {
  id: listView
  model: model
  delegate: delegate
  ScrollBar{}
}
```
* `ListView` 안에 `ScrollBar{}` 넣거나, `ScrollBar{}`의 `flickable`을 View로 세팅하면 됨
