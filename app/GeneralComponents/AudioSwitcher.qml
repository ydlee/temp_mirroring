/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2017 Konsulko Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtWebSockets 1.0


Item {
    id:webSocketComponent

    property string apiString: "agl-service-dasound"
    property var verbs: []
    property string payloadLength: "9999"

    property string fileName : "AudioSwitcher"
    property string currentAudioMode: "aux"

    readonly property var msgid: {
        "call": 2,
        "retok": 3,
        "reterr": 4,
        "event": 5
    }

    function printDebug(fileName, txt) {
        console.log("["+fileName+"] "+ txt)
    }

    function sendSocketMessage(verb, parameter) {
        var requestJson = [ msgid.call, payloadLength, apiString + '/'
        + verb, parameter ]
        printDebug(fileName,"sendSocketMessage: " + JSON.stringify(requestJson))
        verbs.push(verb)
        root.sendTextMessage(JSON.stringify(requestJson))
    }

    function setAudioPath(path)
    {
        var pathName = "audio"
        var modeName = "aux"
        if(path === "audio")
        {
            pathName = "audio"
            modeName = "aux"
        }
        else if(path === "bluetooth")
        {
            pathName = "bluetooth"
            modeName = "bluetooth"
        }
        sendSocketMessage("set_sink", {"sink_name":pathName})
        currentAudioMode = modeName
    }


    function setAudioSwitch()
    {
        if (currentAudioMode === "bluetooth")
        {
            console.log("AUX MODE")
            setAudioPath("audio")
        }

        else if (currentAudioMode === "aux")
        {
            console.log("BT MODE")
            setAudioPath("bluetooth")
        }
    }

    WebSocket {
        id: root
        active: true
        url: bindingAddress

        onTextMessageReceived: {
            var json = JSON.parse(message)
             printDebug(fileName,"Raw response: " + message)
        }

        onStatusChanged: {
            switch (status) {
                case WebSocket.Open:
                printDebug(fileName,"onStatusChanged: Open")
                break

                case WebSocket.Error:
                root.statusString = "WebSocket error: " + root.errorString
                break
            }
        }
    }

}
