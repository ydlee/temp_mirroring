﻿import QtQuick 2.5

Item {
    id: infoPopup

    property string fileName: "NAME"
    property string fileResolution: "RESOLUTION"
    property string fileType: "TYPE"

    property string popup_icon_info: "qrc:/GeneralComponents/Resource/popup_icon_info.png"
    property string popup_video_info_bg: "qrc:/GeneralComponents/Resource/popup_video_info_bg.png"
    property string popup_1btn_pre: "qrc:/GeneralComponents/Resource/popup_1btn_pre.png"
    property string popup_1btn_nor: "qrc:/GeneralComponents/Resource/popup_1btn_nor.png"

    signal closeBtnClicked()

    Rectangle {
        anchors.fill: parent
        color: "#000000"
        opacity: 0.7
    }
    MouseArea {
        anchors.fill: parent

        onClicked: {
        }
    }

    Image {
        x: 250; y: 213; width: 780; height: 301
        source: popup_video_info_bg

        Column {
            spacing: 4
            anchors { top: parent.top; topMargin: 62; left: parent.left; leftMargin: 30 }

            Repeater {
                model: [ fileName, fileResolution, fileType ]
                Text {
                    width: 720; height: 39
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    color: "#ffffff"
                    font.pixelSize: 35
                    text: modelData
                }
            }
        }
        ImageButton{
            id: close
            x: 158; y: 211; width: 464; height: 71
            source: popup_1btn_nor

            onContainsMouseChanged: {
                if (!containsMouse)
                    close.source = popup_1btn_nor
            }
            onPressed: {
                if (containsMouse)
                    close.source = popup_1btn_pre
            }
            onReleased: {
                close.source = popup_1btn_nor

                if (containsMouse)
                    closeBtnClicked()
            }

            Text {
                y: 16; width: 464; height: 39
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: "#ffffff"
                font.pixelSize: 35
                text:  qsTrId("ts_close") + ts.empty
            }
        }
    }
    Image {
        x: 595; y: 168; width: 90; height: 90
        source: popup_icon_info
    }
}

