﻿import QtQuick 2.5

Rectangle{
    id: root

    property alias timer: timer
    property string popup_icon_loading_1: "qrc:/GeneralComponents/Resource/popup_icon_loading_01.png"
    property string popup_icon_loading_2: "qrc:/GeneralComponents/Resource/popup_icon_loading_02.png"
    property string popup_icon_loading_3: "qrc:/GeneralComponents/Resource/popup_icon_loading_03.png"
    property string popup_icon_loading_4: "qrc:/GeneralComponents/Resource/popup_icon_loading_04.png"
    property string popup_icon_loading_5: "qrc:/GeneralComponents/Resource/popup_icon_loading_05.png"
    property string popup_icon_loading_6: "qrc:/GeneralComponents/Resource/popup_icon_loading_06.png"
    property string popup_icon_loading_7: "qrc:/GeneralComponents/Resource/popup_icon_loading_07.png"
    property string popup_icon_loading_8: "qrc:/GeneralComponents/Resource/popup_icon_loading_08.png"
    property string popup_icon_loading_9: "qrc:/GeneralComponents/Resource/popup_icon_loading_09.png"
    property string popup_icon_loading_10: "qrc:/GeneralComponents/Resource/popup_icon_loading_10.png"
    property string popup_icon_loading_11: "qrc:/GeneralComponents/Resource/popup_icon_loading_11.png"

    visible: true
    anchors.fill: parent
    color: "#000000"

    Timer {
        id: timer
        repeat: false
        interval: 800
        onTriggered: {
            root.visible = false
        }
    }

    Image {
        id: icon

        property alias iconTimer: iconTimer

        anchors.centerIn: parent
        width: 90
        height: 90
        asynchronous: true
        cache: false
        source: popup_icon_loading_1

        Timer{
            id: iconTimer

            property int count: 0

            running: root.visible
            repeat: true
            interval: 100
            onTriggered: {
                count++
                if (count == 12)
                    count = 1
                icon.source = root["popup_icon_loading_%1".arg(count)]
            }
        }
    }
}
