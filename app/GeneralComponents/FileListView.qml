﻿import QtQuick 2.5

ListView {
    id: listView
    signal clickedItem(int index)
    property string image_line: "qrc:/GeneralComponents/Resource/image_line.png"
    property string image_list_pre: "qrc:/GeneralComponents/Resource/image_list_pre.png"
    property string list_icon_usb_sel: "qrc:/GeneralComponents/Resource/list_icon_usb_sel.png"
    property string list_icon_usb_nor: "qrc:/GeneralComponents/Resource/list_icon_usb_nor.png"
    property string list_icon_file: "qrc:/GeneralComponents/Resource/list_icon_file.png"
    property string list_icon_folder: "qrc:/GeneralComponents/Resource/list_icon_folder.png"
    property string list_icon_folder_nor: "qrc:/GeneralComponents/Resource/list_icon_folder_nor.png"
    property string list_icon_folder_sel: "qrc:/GeneralComponents/Resource/list_icon_folder_sel.png"
    property string list_icon_video_play: "qrc:/GeneralComponents/Resource/list_icon_video_play.png"
    property string list_icon_playng: "qrc:/GeneralComponents/Resource/list_icon_playng.gif"
    property string list_icon_pause: "qrc:/GeneralComponents/Resource/list_icon_pause.png"

    width: parent.width
    height: parent.height

    boundsBehavior: Flickable.StopAtBounds

    clip: true
    focus: true
    snapMode: ListView.SnapToItem
    highlightFollowsCurrentItem: false
}
