﻿import QtQuick 2.5

Image {
    id: image

    property alias containsMouse: mouseArea.containsMouse
    property alias containsPress: mouseArea.containsPress
    property alias mouseEnabled: mouseArea.enabled
    property bool isNormal: true
    signal clicked()
    signal doubleClicked()
    signal pressAndHold()
    signal pressed()
    signal released()

    enabled: true
    activeFocusOnTab: true
    
    fillMode: Image.PreserveAspectFit
    verticalAlignment: Image.AlignVCenter
    horizontalAlignment: Image.AlignHCenter

    BeepMouseArea {
        id: mouseArea
        anchors.fill: parent

        onClicked: {
            image.clicked()
        }
        onDoubleClicked: {
            image.doubleClicked()
        }
        onPressed: {
            image.pressed()
        }
        onPressAndHold: {
            image.pressAndHold()
        }
        onReleased: {
            image.released()
        }
    }
}
