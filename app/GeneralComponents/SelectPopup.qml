﻿import QtQuick 2.5
import "../GeneralComponents" as GENERAL

Item {
    id: popup
    
    property alias selectList: listView
    property alias titleText: titleText.text
    property string popup_displaytime_bg: "qrc:/GeneralComponents/Resource/popup_displaytime_bg.png"
    property string popup_3btn_nor: "qrc:/GeneralComponents/Resource/popup_3btn_nor.png"
    property string popup_3btn_pre: "qrc:/GeneralComponents/Resource/popup_3btn_pre.png"
    property string popup_radio_btn_nor: "qrc:/GeneralComponents/Resource/popup_radio_btn_nor.png"
    property string popup_radio_btn_sel: "qrc:/GeneralComponents/Resource/popup_radio_btn_sel.png"
    property string popup_radio_btn_bg_nor: "qrc:/GeneralComponents/Resource/popup_radio_btn_bg_nor.png"
    property string popup_radio_btn_bg_sel: "qrc:/GeneralComponents/Resource/popup_radio_btn_bg_sel.png"
    property bool setDefault: true
    property int selected: 0
    property int lastSelect: 0
    
    signal okClicked()
    signal cancelClicked()
    signal listClicked(int selectedIndex)
    
    onOkClicked: {
        visible = false
    }
    onCancelClicked: {
        visible = false
    }

    onListClicked: {
        selected = selectedIndex
    }
    onVisibleChanged: {     // Back button Clicked
        if (selected != lastSelect)
            selected = lastSelect
    }
    
    visible: false
    anchors.fill: parent
    
    Rectangle{
        anchors.fill: parent
        color: "#000000"
        opacity: 0.7
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {}
    }
    
    Image{
        x: 316
        y: 117
        width: 648
        height: 480
        source: popup_displaytime_bg
    }
    Text{
        id: titleText
        x: 336
        y: 142
        width: 608
        height: 39
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: "#ffffff"
        font.pixelSize: 35
    }

    Row {
        x: 331
        y: 514

        Repeater{
            model: [(qsTrId("ts_ok") + ts.empty), (qsTrId("ts_cancel") + ts.empty) ]

            Item{
                width: 314
                height: 71

                ImageButton{
                    id: button
                    width: 304
                    height: 71
                    source: popup_3btn_nor

                    onContainsMouseChanged: {
                        if (!containsMouse)
                            button.source = popup_3btn_nor
                    }
                    onPressed: {
                        if (containsMouse)
                            button.source = popup_3btn_pre
                    }
                    onReleased: {
                        button.source = popup_3btn_nor

                        if (containsMouse) {
                            switch(index) {
                            case 0:
                                lastSelect = selected
                                okClicked()
                                break
                            case 1:
                                cancelClicked()
                                break
                            }
                        }
                    }
                    Text{
                        x: 0
                        y: 16
                        width: 304
                        height: 39
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        color: "#ffffff"
                        font.pixelSize: 35
                        text: modelData
                    }
                }
            }
        }
    }
    
    ListView{
        id: listView
        x: 336
        y: 235
        clip: true
        width: 608
        height: 252
        boundsBehavior: Flickable.StopAtBounds

        delegate: Item{
            width: 608
            height: 63
            
            Image{
                id: radioButtonBg
                x: 0
                y: 0
                width: 608
                height: 57
                source: popup_radio_btn_bg_nor
            }
            Image{
                id: radioButton
                x: 10
                y: 7
                width: 44
                height: 44
                source: popup_radio_btn_nor
            }
            Text{
                x: 74
                y: 9
                width: 524
                height: 39
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                color: "#ffffff"
                font.pixelSize: 35
                text: String(FILE_PATH).replace("Subtitle Off", (qsTrId("ts_subtitle_off")+ts.empty)).replace("Unknown", (qsTrId("ts_unknown")+ts.empty)).replace("(Original)", (qsTrId("ts_original")+ts.empty))     // From FileListModel Class
            }
            BeepMouseArea{
                id: mouseArea
                anchors.fill: parent
                
                onContainsMouseChanged: {
                    if (!containsMouse && index != selected) {
                        radioButtonBg.source = popup_radio_btn_bg_nor
                        radioButton.source = popup_radio_btn_nor
                    }
                }
                onPressed: {
                    if (containsMouse && index != selected) {
                        radioButtonBg.source = popup_radio_btn_bg_sel
                        radioButton.source = popup_radio_btn_sel
                    }
                }
                onReleased: {
                    if (index != selected) {
                        radioButtonBg.source = popup_radio_btn_bg_nor
                        radioButton.source = popup_radio_btn_nor
                    }
                    if (containsMouse)
                        listClicked(index)
                }
            }
            
            states : State {
                name: "default"; when: (index == selected)
                PropertyChanges { target: radioButton; source: popup_radio_btn_sel }
                PropertyChanges { target: radioButtonBg; source: popup_radio_btn_bg_sel }
            }
        }
        GENERAL.ScrollBar{
            scrollBar.anchors.rightMargin: 0
            fastScrollActive: false
        }
    }
}

