﻿import QtQuick 2.5

Item {
    id: root
    property alias slideText: t1.text
    property alias textColor: t1.color
    property bool slide: false
    property int fontPixelSize
    property int spacing: 50
    property int textSize: Math.ceil(t1.paintedWidth)
    property int movingArea: Math.ceil(t1.paintedWidth) - width

    //height: t1.height
    clip: true

    Text {
        id: t1
        font.pixelSize: fontPixelSize
        text: slideText

        SequentialAnimation on x {
            id: animation; loops: Animation.Infinite
            NumberAnimation { from: 0; to: -movingArea; duration: 7500 }
            PauseAnimation { duration: 300 }
            NumberAnimation { from: -movingArea; to: 0; duration: 7500 }
            PauseAnimation { duration: 300 }
        }
    }

    onTextSizeChanged: {
        animation.running = false
        slide = (movingArea > 0) ? true : false

        if (slide) {
            animation.running = true
        }
    }

    states: State{
        name: "center"; when: !slide && !animation.running
        PropertyChanges { target: t1; x: 0; width: root.width; height: root.height; horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter }
    }
}
