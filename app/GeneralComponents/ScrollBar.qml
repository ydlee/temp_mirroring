﻿import QtQuick 2.5

Item {
    id: bar

    property alias scrollBar: scrollBar
    property Flickable flickable: parent
    property int listCount: 0
    property int handleSize: 65
    property int handleHeight: 81
    property var orientation : Qt.Vertical
    property real position: flickable.visibleArea.yPosition
    property real pageSize: flickable.visibleArea.heightRatio
    property bool fastScrollActive: true
    property bool modelCountCheck: (listCount > 24 && fastScrollActive) ? true : false
    property string scroll_bar: "qrc:/GeneralComponents/Resource/scroll_bar.png"
    property string scroll_fast: "qrc:/GeneralComponents/Resource/scroll_fast.png"

    width: 115
    height: flickable.height
    visible: (flickable.visible && pageSize < 1.0) ? true : false
    anchors { top: flickable.top; right: flickable.right; bottom: flickable.bottom; margins: 1 }

    onListCountChanged: {
        if (listCount > 24 && fastScrollActive) {
            handle.visible = true
            timer.restart()
        } else {
            handle.visible = false
            timer.stop()
        }
    }

    Connections {
        target: flickable

        onVisibleChanged: {
            if (flickable.visible) {
                if (listCount > 24 && fastScrollActive) {
                    if (!handle.visible)
                        handle.visible = true
                    timer.restart()
                } else {
                    handle.visible = false
                    timer.stop()
                }
            }
        }
        onMovingChanged: {
            if (flickable.moving) {
                if (listCount > 24 && fastScrollActive) {
                    timer.stop()
                    handle.visible = true
                } else {
                    handle.visible = false
                }
            }
            else {
                timer.restart()
            }
        }
    }

    Timer{
        id: timer
        interval: 3000
        repeat: false
        onTriggered:  {
            handle.visible = false
        }
    }

    Binding {
        target: handle
        property: "y"
        value: (flickable.contentY * clicker.drag.maximumY / (flickable.contentHeight - flickable.height))
        when: (!clicker.drag.active)
    }
    Binding {
        target: flickable
        property: "contentY"
        value: (handle.y * (flickable.contentHeight - flickable.height) / clicker.drag.maximumY)
        when: (clicker.drag.active || clicker.pressed)
    }

    Item {
        id: groove
        clip: true
        anchors {
            fill: parent
            margins: 1
        }

        MouseArea {
            id: clicker
            drag {
                target: handle
                minimumY: 0
                maximumY: (groove.height - handle.height)
                axis: Drag.YAxis
            }
            anchors.fill: handle
            onPressed: {
                timer.stop()
            }
            onReleased: {
                timer.restart()
            }
        }
        Image {
            id: handle

            visible: false; opacity: 1
            width: handleSize
            height: handleHeight
            anchors { right: parent.right; rightMargin: 46 }
            source: scroll_fast

            states: State {
                name: "unactive"; when: !handle.visible
                PropertyChanges { target: handle; anchors.rightMargin: 0; opacity: 0 }
            }
            transitions: Transition {
                from: ""; to: "unactive"; reversible: true
                ParallelAnimation {
                    NumberAnimation { properties: "anchors.rightMargin"; duration: 300; easing.type: Easing.InOutQuad }
                    NumberAnimation { property: "opacity"; duration: 200; easing.type: Easing.InOutQuad }
                }
            }

            onVisibleChanged: {
                scrollBar.visible = !handle.visible
            }
        }
    }

    BorderImage {
        id: scrollBar

        x: 1; y: (position * (flickable.height-2) + 1)
        width: 10
        height: Math.max((pageSize * (flickable.height-2)), 30)
        anchors { right: parent.right; rightMargin: 50 }
        opacity: 1
        border { left: 1; top: 1; right: 1; bottom: 1 }
        horizontalTileMode: BorderImage.Stretch
        verticalTileMode: BorderImage.Stretch
        source: scroll_bar

        states: State {
            name: "unactive"; when: !scrollBar.visible
            PropertyChanges { target: scrollBar; opacity: 0 }
        }
        transitions: Transition {
            from: ""; to: "unactive"; reversible: true
            NumberAnimation { property: "opacity"; duration: 300; easing.type: Easing.InOutQuad }
        }

        MouseArea {
            anchors.fill: parent
            enabled: modelCountCheck

            onClicked: {
                timer.stop()
                handle.visible = true
                timer.restart()
            }
        }
    }
}
