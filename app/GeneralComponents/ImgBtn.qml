import QtQuick 2.6
import QtQuick.Layouts 1.1

Item {
    id: imgbtn_item
    //color: "transparent"

    property string text: value

    property string imgsrc: ""
    property string selectedImgSrc: ""
    property string pushedImgSrc: ""
    property string disableImgSrc: ""


    activeFocusOnTab: true
    enabled: true

    signal buttonClick()
    height: parent.height
    Component.onCompleted:
    {



    }

    Rectangle{
       anchors.fill: imgbtn_item
       color: "transparent"
       Image {
           id: img
           width: parent.width
           height: parent.height
           source: imgsrc
           fillMode: Image.PreserveAspectFit
           verticalAlignment: Image.AlignVCenter
           horizontalAlignment: Image.AlignHCenter

       }
       Text {
           id: txt
           width: parent.width
           height: parent.height

           verticalAlignment: Image.AlignVCenter
           horizontalAlignment: Image.AlignHCenter

       }

       BeepMouseArea {
           id: mouse
           width: parent.width
           height: parent.height

           onClicked: {
               if(imgbtn_item.enabled)
                    buttonClick();
           }
       }
   }

   states: [
       State { //Normal
           when: (!imgbtn_item.focus) && (imgbtn_item.enabled) && (!mouse.pressed)
           PropertyChanges { target: img; source: imgsrc }
       },
       State {//Pushed
           when: (!imgbtn_item.focus) && (imgbtn_item.enabled) && (mouse.pressed)
           PropertyChanges { target: img; source: pushedImgSrc }
       },
       State {//Focus
           when: (imgbtn_item.focus) && (imgbtn_item.enabled) && (!mouse.pressed)
           PropertyChanges { target: img; source: selectedImgSrc }
       },
       State {//Disable
           when: (!imgbtn_item.enabled) && (!mouse.pressed);
           PropertyChanges { target: img; source: disableImgSrc }
       }
   ]



}
