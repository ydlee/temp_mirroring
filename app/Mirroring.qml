import QtQuick 2.5
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0

import "./GeneralComponents"

Window {
    property bool isShow: false

    id:main_screen
    property alias popup: popup
//    property alias popup: main_screen.popup
    visible: true
    width: container.width * container.scale
    height: container.height * container.scale
    flags: Qt.FramelessWindowHint

    Image {
        anchors.fill: parent
        source: theme.image.bg
        visible: (backend.isState == 0) || (backend.isState == 1) || (backend.isState == 3) || (backend.isState == 4)
    }

    Rectangle {
        id: container
        anchors.centerIn: parent
        width: Screen.width
        height: Screen.height
        color: (backend.isBG == 1) ? "#000000" : "#112233"

        //opacity: 0.5
        visible: (backend.isState == 2)
    }

    TitleBar {
        id: titlebar
        title_width: parent.width
        title_height: parent.height * 0.13
        title_txt: qsTrId("ts_mirroing") + ts.empty
        visible: (backend.isState == 0) || (backend.isState == 1) || (backend.isState == 3) || (backend.isState == 4)
        enabled: (backend.isState == 0) || (backend.isState == 1) || (backend.isState == 3) || (backend.isState == 4)
        //title_txt: backend.phoneName.toString()+"testtest"
    }

    Control {
        id: control
        anchors.fill: parent
        visible: (backend.isState == 0) || (backend.isState == 1) || (backend.isState == 3) || (backend.isState == 4)
        enabled: (backend.isState == 0) || (backend.isState == 1) || (backend.isState == 3) || (backend.isState == 4)
    }

    Bottombar {
        y : Screen.height - height
    }

    Timer {
        id: myTimer
        interval: 3000
        running: (backend.isState == 3)
        repeat: true
        onTriggered:{
            isShow = true;
            backend.isState = 2;
        }
    }

    /*
    ThresholdMask {
        anchors.fill: parent
        source: control
        maskSource: container
        threshold: 0.8
        spread: 0.8
    }
    */

    Popup {
        id: popup
    }

}
