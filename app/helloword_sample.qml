import QtQuick 2.4
import QtQuick.Window 2.2

Window {
    id: window
//    flags: Qt.FramelessWindowHint
    width : 1280; height: 800-200
    visible: true

    Item{
        anchors.fill: parent
        Rectangle{
            id : rect1
            color: "gray"
            anchors.fill: parent
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    LDS.num += 1
                }
                onDoubleClicked:{
                    console.debug("onDoubleClicked");
//                    window.visible = false
                }
            }

            Text {
                id : txt1
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 50

                text: qsTr("Hello World ") + LDS.num
                anchors.centerIn: parent
                onTextChanged: {
                    console.log("qml textchagned")
                }
            }

        }
    }

}
