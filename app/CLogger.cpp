///////////////////////////////////////////////////////////
//  CLogger.cpp
//  Implementation of the Class CLogger
//  Created on:      28-7-2020 오후 2:48:41
//  Original author: user
///////////////////////////////////////////////////////////

#include "CLogger.h"
#include <cstdarg>
#include <string>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdarg.h>
#include "CUtils.h"

using namespace std;


CLogger* CLogger::m_pLogThis = NULL;

CLogger::CLogger()
{
#if DEBUG_TEST_ENABLE
    if(strcmp(m_pstrContext,"") == 0)
        DLT_REGISTER_APP("NON", "First Play");
    else
        DLT_REGISTER_APP(m_pstrContext, "First Play");

    DLT_REGISTER_CONTEXT(context_between, "BETW", "Between");
    DLT_REGISTER_CONTEXT(context_play, "PLAY", "Play context");
    DLT_REGISTER_CONTEXT(context_stop, "STOP", "Stop context");
#endif

}

CLogger::~CLogger(){
//     m_pLogger;
#if 0//LINUX_ARM7_ENABLE
    DLT_UNREGISTER_CONTEXT(context_between);
    DLT_UNREGISTER_CONTEXT(context_stop);
    DLT_UNREGISTER_CONTEXT(context_play);
    DLT_UNREGISTER_APP();
#endif
}



CLogger* CLogger::Instance(){

   if( nullptr == m_pLogThis){
        m_pLogThis = new CLogger();
#if DEBUG_TEST_TEMP_ENABLE
//         m_Logfile.open(m_sFileName.c_str(), ios::out | ios::app);
#endif
   }
    return  m_pLogThis;
}


char* CLogger::log(const char* format, ...)
{
        size_t size = 500;
        char* result = (char*)malloc(size);
        if (!result) return NULL; // error handling!
        while (1) {
            va_list ap;
            va_start(ap, format);
            size_t used = vsnprintf(result, size, format, ap);
            va_end(ap);
            char* newptr = (char*) realloc(result, size);
            if (!newptr) { // error
                free(result);
                return NULL;
            }
            result = newptr;
            if (used <= size) break;
            size = used;
        }

        return result;

}

void CLogger::Log(const char * format, ...)
{
    qDebug() << "CLogger::Log(const char * format, ...)" ;
#if 0
    char msg[2048]={0,};
     va_list argp;
     va_start(argp, format);
     vsnprintf(msg, 2048, format, argp);
     va_end(argp);
     logDebug(QString(msg));
//-------------------------------------
#else
    va_list args;
    va_start(args, format);

    QString message;
    message.vsprintf(format, args);

    va_end(args);

    message = message.trimmed();
    logDebug(message);

#endif
}


void CLogger::logInfo(QString qstrMsg)
{
#if DEBUG_TEST_ENABLE
    DLT_LOG(context_play, DLT_LOG_INFO, DLT_STRING(strMsg));
#else
    qDebug() << "CLogger::logInfo";
    qInfo() << qstrMsg;
#endif

}

void CLogger::logDebug(QString qstrMsg)
{
 #if DEBUG_TEST_ENABLE
    DLT_LOG(context_play, DLT_LOG_DEBUG, DLT_STRING(strMsg));
#else
    qDebug() << "CLogger::logDebug";
    qDebug() << qstrMsg;
#endif
}

#if DEBUG_TEST_TEMP_ENABLE
//>> logMe.logging("log this msg", a, ",", b);
template <typename ... Args >
void CLogger::logging(Args ... args)
{
      std::stringstream ss;
      std::initializer_list<int> unused{ (ss << args, 0)... };

    //  Log4cpp::Category& myLogger = log4cpp::Category::getRoot();
    //  myLogger.log(log4cpp::Priority::INFO, ss.str());

}
#endif


