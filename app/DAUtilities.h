#ifndef DA_UTILITIES_H
#define DA_UTILITIES_H

#include <QObject>
#include <QString>
#include <QStringList>

//return value: true is contain, false is no contain
bool commandFilter(QString jsonString, QStringList list);

QString commandUpdateDurationFilter(QString jsonString);
QString replaceJson(QString jsonString, bool tf = false);



QString getJsonValue(QString jsonString, QString key = "COMMAND");
QString setJsonValue(QString jsonString, QString val); //default key is "COMMAND"
QString setJsonValue(QString jsonString, QString key, QString val);




#endif // DA_UTILITIES_H
