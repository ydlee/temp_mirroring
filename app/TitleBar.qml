import QtQuick 2.5

Image {

    id: title_bar
    property real title_height
    property real title_width
    property string title_txt

    width: title_width
    height: title_height
    source: theme.image.top_bar_bg

    Text {
        id: titlebar_text
        text: title_txt
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        width: title_width * 0.3
        height: title_height
        color: "white"
        font.pixelSize: 35
        x: width * 0.1
    }
}
