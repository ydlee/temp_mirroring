///////////////////////////////////////////////////////////
//  CUtils.cpp
//  Implementation of the Class CUtils
//  Created on:      28-7-2020 오후 2:46:24
//  Original author: user
///////////////////////////////////////////////////////////

#include "CUtils.h"
#include <ctime>
#include <QDir>
#include "AppMirroring.h"

CUtils::CUtils(){
    init();
}


CUtils::~CUtils(){

}

void CUtils::init(){

//    getFilelist();
    getScriptPython();

}

// CFilelist* CUtils::getFilelist()
// {
////     if(nullptr ==  m_pFilelist)
////           m_pFilelist = new CFilelist();
//     return m_pFilelist;
// }

 CScriptPython* CUtils::getScriptPython()
 {
     if(nullptr ==  m_pScriptPython)
           m_pScriptPython = new CScriptPython();
     return m_pScriptPython;
 }

  QString CUtils::CurrentDateTime()
  {

#if 0
    auto start = std::chrono::system_clock::now();
    // Some computation here
    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    std::cout << "finished computation at " << std::ctime(&end_time)
           << "elapsed time: " << elapsed_seconds.count() << "s\n";
#endif

    return  QString("time");
}

QString CUtils::currentPath()
{
    return QDir::currentPath();
}


//Get Event_OnScreenReply "no"
int CUtils::run_Engine(QProcess*  process)
{
//    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;


    if(process->state() != QProcess::NotRunning)
    {
        process->kill();
        process->waitForFinished();
    }

    QString command;

//    switch (m_isSelectMode)
//    {
//        //Android
//        case 0:
//            command = "/home/root/kivic/wfdp2p.sh";

//            break;
//        //iPhone
//        case 1:
//            command = "/usr/bin/kivicmain";

//            break;
//        default:
//            break;
//    }

    qDebug() << __FUNCTION__ << command;

    process->start(command);

    QJsonObject jsonObj;
//    jsonObj.insert("selectedMode",QJsonValue::fromVariant(m_isSelectMode));
//    m_setup->set_target("mirr",jsonObj);
    qDebug() << "**************";

   AppMirroring* pApp = ((AppMirroring*)this);
    pApp->setIsState(CONNETTING);

    return 0;
}


int CUtils::get_Phonename()
{
//    qDebug() << "[SY]" << __FUNCTION__ << "m_isState : " << m_isState << "m_isSelectMode" << m_isSelectMode << "/ m_isBG : " << m_isBG;

    QProcess *process = new QProcess();
    int nResult = 0;

//    if(process->state() != QProcess::NotRunning)
//    {
//        process->kill();
//        process->waitForFinished();
//    }

    QString command = "/usr/sbin/wpa_cli p2p_peers";

    process->start(command);
    process->waitForFinished(-1);

    //QByteArray arr;
    //arr = process->readAllStandardOutput();
    //qDebug() << "************** " << arr;
    process->setReadChannel(QProcess::StandardOutput);
    QByteArray arr;
    while (process->canReadLine())
    {
       arr = process->readLine();

    }
    //qDebug() << "====================== ";
    //qDebug() << arr;
    //qDebug() << "====================== ";

    QString mac = QString::fromUtf8(arr);
    mac.remove(mac.size()-1, 1);

    //qDebug() << "====================== ";
    //qDebug() << mac;
    //qDebug() << "====================== ";

    QString commnad2 = "/usr/sbin/wpa_cli p2p_peer";
    QString command2full = commnad2 + " " + mac;
    //qDebug() << "**************";
    //qDebug() << command2full;
    //qDebug() << "**************";
    QProcess *process2 = new QProcess();
//    if(process2->state() != QProcess::NotRunning)
//    {
//        process2->kill();
//        process2->waitForFinished();
//    }

    process2->start(command2full);
    process2->waitForFinished(-1);

    process2->setReadChannel(QProcess::StandardOutput);
    QByteArray arr2;
    QString phone_name;
    while (process2->canReadLine())
    {
       arr2 = process2->readLine();
       phone_name = QString::fromUtf8(arr2);
       bool ret = phone_name.contains("device_name", Qt::CaseInsensitive);
       if(ret)
       {
           QStringList list = phone_name.split("=");
           qDebug() << "**************";
           qDebug() << list[1];
           qDebug() << "**************";
           //m_Phonename = list[1];
//           setPhoneName(list[1]);
           nResult = 1;
           break;
       }

    }

    return nResult;

}
