﻿/***********************************
 * 
 * Copyright (C) 2017 Drimaes
 *
 *
 * L.E.P 2020/07/31
 ************************************/

#include "sharddata.h"
#include "AppMirroring.h"

int main(int argc, char *argv[])
{
  
    AppMirroring app(argc, argv, QString("Mirroring") );
    app.showqml();

    return app.exec();
}
