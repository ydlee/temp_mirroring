///////////////////////////////////////////////////////////
//  CIPCService.cpp
//  Implementation of the Class CIPCService
//  Created on:      30-7-2020 오후 12:20:44
//  Original author: user
///////////////////////////////////////////////////////////

#include "CIPCService.h"


CIPCService::CIPCService()
    :m_url(nullptr)
{
    //TODO
	 init();
}


CIPCService::~CIPCService(){
    //TODO
//#if LINUX_ARM7_ENABLE
//    delete m_windowManager;
//    delete m_homeScreen;
//#endif
}

void CIPCService::init(){
    //TODO
#if LINUX_ARM7_ENABLE
//    m_windowManager = new QLibWindowmanager();
//    m_homeScreen    = new LibHomeScreen();

#endif

}
#if LINUX_X86_ENABLE
int CIPCService::echoClient(const QUrl &url) {
    m_webSocket.open(QUrl(url));
    return 0;
}

int CIPCService::openWebClient(QWebSocket& webSocket,const QUrl &url) {
    if(url.isEmpty() == false && url.isValid() == true) {
        webSocket.open(QUrl(url));
        qDebug() << "webSocket state:" << webSocket.state();
        return (int)webSocket.state();
    }
    return -1;;
}

bool CIPCService::getUrlAddressInit(QWebSocket& webSocket,QUrl& url) {

    int nReuslt = openWebClient(webSocket,url);
    if( -1 == nReuslt )
        return false;
    else
        return true;
}

bool CIPCService::getUrlAddressInit(QWebSocket& webSocket,int port, const char *chSecret) {
    qDebug() << "port:" << port  << "secret:" << chSecret;

   if( 0 < port  && strcmp(chSecret,"") !=0 )
   {
        QUrl url = getUrlAddressInit(port,chSecret);
        if(url.isEmpty() == false && url.isValid() == true) {
        return getUrlAddressInit(webSocket,url);
      }
   }
    return false;
}

#endif

QUrl CIPCService::getUrlAddressInit(int port, const char *chSecret) {

    qDebug() << "port:" << port  << "secret::" << chSecret;
    QUrl url;
    QString strIP = QString("127.0.0.1");

    if( 0 < port  && strcmp(chSecret,"") !=0 )
    {
        int nPort = port;
        QString qstrSecret = QString(chSecret);
        url.setScheme(QStringLiteral("ws"));
        url.setHost(QStringLiteral("%1").arg(strIP));
        url.setPort(nPort);
        url.setPath(QStringLiteral("/api"));

        QUrlQuery query;
        query.addQueryItem(QStringLiteral("token"), qstrSecret);
        url.setQuery(query);

        if(url.isEmpty() == false && url.isValid() == true)
            return url;
    }

    return url;
}





