TARGET = app_mirroring
QT = quick websockets multimedia

CONFIG += link_pkgconfig c++11
#PKGCONFIG += libhomescreen qlibwindowmanager

#RESOURCES += \
#    mirroring.qrc \
#    GeneralComponents/sub.qrc

RESOURCES += \
       main_sample_qml.qrc

DEFINES += LINUX_ARM7_ENABLE=0 \
           LINUX_X86_ENABLE=1 \

DEFINES += TARGET_STR=\\\"$$TARGET\\\"

include(app.pri)

#LIBS += -lhomescreen
#        -lqtappfw


HEADERS += \
        AppMirroring.h \
        CFormCtrl.h \
        CFormView.h \
        CUtils.h \
        CCommon.h \
        CBrokerEvent.h \
        CLogger.h \
        CIPCService.h \
        CThirdPartyLib.h  \
        CBeep.h \
        CBottombar.h \
        CScriptLanguage.h \
        CPopup.h \
        CScriptPython.h \
        sharddata.h \
        CLibMirroring.h \
        fileio.h \
        #    backend.h \
    #    DAIpcSocket.h \
    #    DAUtilities.h \
    #    GeneralComponents/setupservice.h \
    #    GeneralComponents/theme.h \
    #   GeneralComponents/resource.h
    
SOURCES += \
        main.cpp \
        AppMirroring.cpp \
        CFormCtrl.cpp \
        CFormView.cpp \
        CUtils.cpp \
        CCommon.cpp \
        CBrokerEvent.cpp \
        CLogger.cpp \
        CIPCService.cpp \
        CThirdPartyLib.cpp \
        CBeep.cpp \
        CBottombar.cpp \
        CScriptPython.cpp \
        CScriptLanguage.cpp \
        CPopup.cpp \
        CLibMirroring.cpp \
         fileio.cpp \
        #    backend.cpp \
        #    DAUtilities.cpp \
        #    DAIpcSocket.cpp \
        #    GeneralComponents/setupservice.cpp \
        #    GeneralComponents/theme.cpp


