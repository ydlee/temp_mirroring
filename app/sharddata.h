#ifndef __SHARDDATA_H__
#define __SHARDDATA_H__ 

#include <QObject>


//ignore
#define  DEBUG_VISIBLE                      0
#define  DEBUG_TEST_ENABLE                  0
#define  DEBUG_TEST_TEMP_ENABLE             0
#define  DEBUG_TEST_QML_TEMP_ENABLE         1
//>> file [app_x64.pro] ,or [app.pro]
//#define  LINUX_X86_ENABLE                   0
//#define  LINUX_ARM7_ENABLE                  1

//>> Mirroing
//===============================================
#define NOTCONNECT  0
#define CONNETTING  1
#define CONNECTED   2
#define SHOW_PHONENAME_BEFORECONNECTED   3
#define GOHOME   4

#define NOTBACKGROUND     0
#define BACKGROUND        1

#define MODE_NONE           0
#define MODE_VISIBLE        1
#define MODE_INVISIBLE      2

#define ANDROUD         0
#define IPHONE          1

//===============================================

//Q_OS_LINUX

typedef struct MyStruct
{
    int nPort;
    QString qstrSecret;

}stuWebSocket;

#endif // SHARDDATA_H
