#include "DAIpcSocket.h"

#include <QDebug>



DAIpcSocket::DAIpcSocket(int mode) : QThread()
{

    strcpy(m_sockLocalFile, SOCK_LOCALFILE);
    strcpy(m_sockRemoteFile, SOCK_REMOTEFILE);

}

DAIpcSocket::~DAIpcSocket()
{

}

void DAIpcSocket::run()
{
    QSocketNotifier *notifier = new QSocketNotifier(m_socket, QSocketNotifier::Read, this);

    connect(notifier, SIGNAL(activated(int)), this, SLOT(onReceive(int)));
    QThread::exec();
}

//
//  init[Parts]IpcSocket
//


void DAIpcSocket::initIpcSocket()
{
    if (access(m_sockLocalFile, F_OK) <= 0)
    {
        unlink(m_sockLocalFile);
    }

    m_socket = socket(PF_FILE, SOCK_DGRAM, 0);

    if (m_socket < 0)
    {
        exit(1);
    }

    memset(&m_LocalAddress, 0, sizeof(m_LocalAddress));
    m_LocalAddress.sun_family = AF_UNIX;
    strcpy(m_LocalAddress.sun_path, m_sockLocalFile);
    if(bind(m_socket, (struct sockaddr*)&m_LocalAddress, sizeof(m_LocalAddress)) < 0)
    {
        qDebug("bind() error\n");
        exit(1);
    }

    memset(&m_RemoteAddress, 0, sizeof(m_RemoteAddress));
    m_RemoteAddress.sun_family = AF_UNIX;
    strcpy(m_RemoteAddress.sun_path, m_sockRemoteFile);
}


//
//  [Do]Message
//
void DAIpcSocket::sendMessage(QString message)
{
    sendto(m_socket, message.toStdString().c_str(), strlen(message.toStdString().c_str()), 0, (struct sockaddr*)&m_RemoteAddress, sizeof(m_RemoteAddress));
}


//
//  on[Parts]Receive
//
void DAIpcSocket::onReceive(int /*tmp*/)
{
   int szRead;
   m_sizeAddress  = sizeof(m_RemoteAddress);
   memset(m_bufferReceive, 0, BUFFER_SIZE);
   szRead = recvfrom(m_socket, m_bufferReceive, BUFFER_SIZE, 0 , (struct sockaddr*)&m_RemoteAddress, &m_sizeAddress);

   QString tmp;
   tmp.append(m_bufferReceive);

   emit receiveMessage(tmp);
}
